#
# Functions and class definitions for the Ortho/Para and deuteration chemical network
# used for the chemical clock testing project. The network is from Oli (Sipila 2015).
#
# Here we are dealing with conversion of the network to the ALCHEMIC format.
# This involves changing notations for species and adding explicitly FREEZE and
# DESORB keywords/processes to the network.
#

from cycler import cycler
import numpy as np
import os
import copy
import matplotlib.pyplot as plt
import string
import sys

class ChemModl:
# Define the network structure, we need reactants, products, 3 coefficients and
# some space for potential comments.
#
    def __init__(self, **kwargs):
# Phisical conditions as function of time
        self.x = kwargs.get('x',None)
        self.y = kwargs.get('y',None)
        self.z = kwargs.get('z',None)
        time = kwargs.get('time',None)
        self.time = time
        self.tindex = np.arange(len(time))
        self.rho = kwargs.get('rho',None)
        self.Tg = kwargs.get('Tg',None)
        self.Td = kwargs.get('Td',None)
        self.G0 = kwargs.get('G0',None)
        self.AvIS = kwargs.get('AvIS',None)
        self.AvSt = kwargs.get('AvSt',None)
        self.fH2_IS = kwargs.get('fH2_IS',None)
        self.fH2_St = kwargs.get('fH2_St',None)
        self.fCO_IS = kwargs.get('fCO_IS',None)
        self.fCO_St = kwargs.get('fCO_St',None)
        self.ZetaCR = kwargs.get('ZetaCR',None)
        self.ZetaX = kwargs.get('ZetaX',None)
        self.gdens = kwargs.get('gdens',None)
        self.amu = kwargs.get('amu',None)
# Abundances as function of time for each species
        spec = kwargs.get('spec',None)
        self.ns = len(spec)
        self.spec = spec
        self.sindex = np.arange(len(spec))
        self.abuns = kwargs.get('abuns',None)
# Reactions and their rates as function of time
        self.InitSpec = kwargs.get('InitSpec',None)
        self.InitAbuns = kwargs.get('InitAbuns',None)
        nre = len(kwargs.get('r1',''))
        self.rindex = np.arange(nre)
        self.nre = nre
        self.r1 = kwargs.get('r1',None)
        self.r2 = kwargs.get('r2',None)
        self.p1 = kwargs.get('p1',None)
        self.p2 = kwargs.get('p2',None)
        self.p3 = kwargs.get('p3',None)
        self.p4 = kwargs.get('p4',None)
        self.p5 = kwargs.get('p5',None)
        self.ak = kwargs.get('ak',None)
# Metadata
        self.specfile = kwargs.get('specfile',None)
        self.ratefile = kwargs.get('ratefile',None)
        self.ID = kwargs.get('ID',None)
        self.rhod = kwargs.get('rhod',None)
        self.mdmg = kwargs.get('mdmg',None)
        self.albedo_UV = kwargs.get('albedo_UV',None)

    def PlotPhys(self, xlog=0):
        mp = 1.6726e-24        # Mass of proton  [g]
        if (xlog == 0):
            time = self.time/1e6
            plt.xlabel("t [Myr]",size="medium")
        else:
            time = self.time
            plt.xscale('log')
            plt.xlabel("t [yr]",size="medium")
        time = self.time
# Density
        plt.subplot(311)
        plt.title("Physical conditions with time")
        try:
            datY = self.rho/(self.amu*mp)
        except:
            datY = self.rho/(1.4*mp)
        if ( type(datY) == float ):
            datY = np.zeros(len(time)) + datY
        plt.plot(time, datY, "g-",linewidth=2.0)
        plt.yscale('log')
        plt.ylabel("$n_{tot}\,[cm^{-3}]$",size="medium")
        if (datY.min() != datY.max()):
            plt.axis([time.min(), time.max(), datY.min(), datY.max()])
        plt.grid(True)
# Temperatures
        plt.subplot(312)
        datY0 = self.Tg
        datY1 = self.Td
        if ( type(datY0) == float ):
            datY0 = np.zeros(len(time)) + datY0
            datY1 = np.zeros(len(time)) + datY1
        plt.plot(time,datY0,"r-", label="$T_{gas}$", linewidth=2.0)
        plt.plot(time,datY1,"b-", label="$T_{dust}$", linewidth=2.0)
        plt.legend(loc=0)
        plt.ylabel("$T [K]$",size="medium")
        if (datY.min() != datY.max()):
           plt.axis([time.min(), time.max(), 0, max([datY0.max(),datY1.max()])])
        plt.grid(True)
# Mean Av
        plt.subplot(313)
        datY = self.AvIS
        if ( type(datY) == float ):
            datY = np.zeros(len(time)) + datY
        plt.plot(time,datY, "y-", label="$<A_{V}>$", linewidth=2.0)
        plt.legend(loc=4)
        plt.ylabel("$A_{V} [mag]$",size="medium")
        plt.xlabel("t [Myr]",size="medium")
        if (datY.min() != datY.max()):
            plt.axis([time.min(), time.max(), datY.min(), datY.max()])
        plt.grid(True)

        plt.show()

    def PlotAbuns(self,species=["H2","C+","C","CO"],title='',yrange=[1e-10,1e0],xlog=0,marker='',markevery=1,show=0,oplot=False,legcol=1):
        mp = 1.6726e-24        # Mass of proton  [g]
        if not oplot:
            plt.figure(figsize=(7,5), dpi=120)
        plt.rc('axes', prop_cycle=(cycler('color', ["b","g","r","c","m","y","k"]) ))
        if title == '':
            title = "Abundances of "+string.join(species,", ")
        plt.title(title)
        # Search the present species
        if (xlog == 0):
            time = self.time/1e6
            plt.xlabel("t [Myr]",size="medium")
        else:
            time = self.time
            plt.xscale('log')
            plt.xlabel("t [yr]",size="medium")

        plt.axis([time.min(), time.max(), yrange[0], yrange[1]])

        for i in range(len(species)):
            try:
                if "_+_" in species[i]:
                    Specs = species[i].split("_+_")
                    Abun2Plot = np.zeros(len(self.abuns[0,:]))
                    for bb in range(len(Specs)):
                        ind1 = self.spec.index(Specs[bb])
                        Abun2Plot = Abun2Plot + self.abuns[ind1,:]
                else:
                    ind1 = self.spec.index(species[i])
                    Abun2Plot = self.abuns[ind1,:]
                plt.plot(time,Abun2Plot,linewidth=2.0,label=species[i],marker=marker,markevery=markevery)
            except:
                print "Species " + species[i] + " not found in model!"

        plt.yscale('log')
        plt.ylabel("Fractional abundance",size="medium")
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.,ncol=legcol)
        plt.grid(True)

        if (show == 1):
            plt.show()

    def PlotRates(self,species=["H2"],title='',yrange=[1e-16,1e-8],xlog=0,marker='',markevery=1,show=0,oplot=False,legcol=1):
        print "PlotRates() function is not yet available!"
        return 1

class network:

# Define the network structure, we need reactants, products, 3 coefficients and
# some space for potential comments.
#
    def __init__(self, index, r1, r2, p1, p2, p3, p4, p5, alpha, beta, gamma, rtype, formul, comment):
        self.index = index
        self.r1 = r1
        self.r2 = r2
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
        self.p4 = p4
        self.p5 = p5
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma
        self.comment = comment
# Additional data, calculated from the above data
#    1) find the uniq species
        TMPspecies = set( np.concatenate([self.r1,self.r2,self.p1,self.p2,
                                          self.p3,self.p4,self.p5],axis=0) )
#        TMPspecies.discard('')
#        TMPspecies.discard('           ')

        self.species = list( TMPspecies )

#    2) Convert to ALCHEMIC nomenclature
        self.grain2alchemic()
        self.amass = np.zeros( len(self.species) )
        self.anum  = np.zeros( len(self.species) )
        self.count_atoms()
#    3) Set the reaction type according OSU (or KIDA)
        self.rtype  = rtype   #np.zeros( len(self.r1) )
        self.formul = formul

# GRAIN2ALCHEMIC
#
# Function to convert the {species}* notation for grain surface species that
# Oli is using to g{species} notation that we are using in ALCHEMIC
#
# Conversions to be done:
#
#       {species}*  -->  g{species}
#        e-         -->  ELECTR
#        g{-/+/0}   -->  G{-/+/0}
#

    def grain2alchemic(self):

        for i in range( len( self.r1 ) ):

# Strip the witespaces
            self.r1[i] = self.r1[i].strip()
            self.r2[i] = self.r2[i].strip()
            self.p1[i] = self.p1[i].strip()
            self.p2[i] = self.p2[i].strip()
            self.p3[i] = self.p3[i].strip()
            self.p4[i] = self.p4[i].strip()
            self.p5[i] = self.p5[i].strip()

            if ( '*' in self.r1[i] ):
                self.r1[i] = 'g' + self.r1[i].replace('*','')
            if ( '*' in self.r2[i] ):
                self.r2[i] = 'g' + self.r2[i].replace('*','')
            if ( '*' in self.p1[i] ):
                self.p1[i] = 'g' + self.p1[i].replace('*','')
            if ( self.p2[i] != '' ):
                if ( '*' in self.p2[i] ):
                    self.p2[i] = 'g' + self.p2[i].replace('*','')
            if ( self.p3[i] != '' ):
                if ( '*' in self.p3[i] ):
                    self.p3[i] = 'g' + self.p3[i].replace('*','')
            if (self.p4[i] != ''):
                if ( '*' in self.p4[i] ):
                    self.p4[i] = 'g' + self.p4[i].replace('*','')
            if (self.p5[i] != ''):
                if ( '*' in self.p5[i] ):
                    self.p5[i] = 'g' + self.p5[i].replace('*','')
# Electrons
            if ( 'e-' in self.r1[i] ):
                self.r1[i] = self.r1[i].replace('e-','ELECTR')
            if ( 'e-' in self.r2[i] ):
                self.r2[i] = self.r2[i].replace('e-','ELECTR')
            if ( 'e-' in self.p1[i] ):
                self.p1[i] = self.p1[i].replace('e-','ELECTR')
            if ( self.p2[i] != '' ):
                if ( 'e-' in self.p2[i] ):
                    self.p2[i] = self.p2[i].replace('e-','ELECTR')
            if ( self.p3[i] != '' ):
                if ( 'e-' in self.p3[i] ):
                    self.p3[i] = self.p3[i].replace('e-','ELECTR')
            if (self.p4[i] != ''):
                if ( 'e-' in self.p4[i] ):
                    self.p4[i] = self.p4[i].replace('e-','ELECTR')
            if (self.p5[i] != ''):
                if ( 'e-' in self.p5[i] ):
                    self.p5[i] = self.p5[i].replace('e-','ELECTR')

# Grains (G0,G+,G-)
            if ( self.r1[i] == 'g'):
                self.r1[i] = 'G0'
            if ( self.r1[i] == 'g+'):
                self.r1[i] = 'G+'
            if ( self.r1[i] == 'g-'):
                self.r1[i] = 'G-'

            if ( self.r2[i] == 'g'):
                self.r2[i] = 'G0'
            if ( self.r2[i] == 'g+'):
                self.r2[i] = 'G+'
            if ( self.r2[i] == 'g-'):
                self.r2[i] = 'G-'

            if ( self.p1[i] == 'g'):
                self.p1[i] = 'G0'
            if ( self.p1[i] == 'g+'):
                self.p1[i] = 'G+'
            if ( self.p1[i] == 'g-'):
                self.p1[i] = 'G-'

            if ( self.p2[i] == 'g'):
                self.p2[i] = 'G0'
            if ( self.p2[i] == 'g+'):
                self.p2[i] = 'G+'
            if ( self.p2[i] == 'g-'):
                self.p2[i] = 'G-'

            if ( self.p3[i] == 'g'):
                self.p3[i] = 'G0'
            if ( self.p3[i] == 'g+'):
                self.p3[i] = 'G+'
            if ( self.p3[i] == 'g-'):
                self.p3[i] = 'G-'

            if ( self.p4[i] == 'g'):
                self.p4[i] = 'G0'
            if ( self.p4[i] == 'g+'):
                self.p4[i] = 'G+'
            if ( self.p4[i] == 'g-'):
                self.p4[i] = 'G-'

            if ( self.p5[i] == 'g'):
                self.p5[i] = 'G0'
            if ( self.p5[i] == 'g+'):
                self.p5[i] = 'G+'
            if ( self.p5[i] == 'g-'):
                self.p5[i] = 'G-'

# Convert the {Mg} notation to {MG} to avoid degeneracy of the {g} (i.e. grain species)\
            self.r1[i] = self.r1[i].replace('Mg','MG')
            self.r2[i] = self.r2[i].replace('Mg','MG')
            self.p1[i] = self.p1[i].replace('Mg','MG')
            self.p2[i] = self.p2[i].replace('Mg','MG')
            self.p3[i] = self.p3[i].replace('Mg','MG')
            self.p4[i] = self.p4[i].replace('Mg','MG')
            self.p5[i] = self.p5[i].replace('Mg','MG')

            self.r1[i] = self.r1[i].replace('He','HE')
            self.r2[i] = self.r2[i].replace('He','HE')
            self.p1[i] = self.p1[i].replace('He','HE')
            self.p2[i] = self.p2[i].replace('He','HE')
            self.p3[i] = self.p3[i].replace('He','HE')
            self.p4[i] = self.p4[i].replace('He','HE')
            self.p5[i] = self.p5[i].replace('He','HE')

            self.r1[i] = self.r1[i].replace('Fe','FE')
            self.r2[i] = self.r2[i].replace('Fe','FE')
            self.p1[i] = self.p1[i].replace('Fe','FE')
            self.p2[i] = self.p2[i].replace('Fe','FE')
            self.p3[i] = self.p3[i].replace('Fe','FE')
            self.p4[i] = self.p4[i].replace('Fe','FE')
            self.p5[i] = self.p5[i].replace('Fe','FE')

            self.r1[i] = self.r1[i].replace('Si','SI')
            self.r2[i] = self.r2[i].replace('Si','SI')
            self.p1[i] = self.p1[i].replace('Si','SI')
            self.p2[i] = self.p2[i].replace('Si','SI')
            self.p3[i] = self.p3[i].replace('Si','SI')
            self.p4[i] = self.p4[i].replace('Si','SI')
            self.p5[i] = self.p5[i].replace('Si','SI')

            self.r1[i] = self.r1[i].replace('Na','NA')
            self.r2[i] = self.r2[i].replace('Na','NA')
            self.p1[i] = self.p1[i].replace('Na','NA')
            self.p2[i] = self.p2[i].replace('Na','NA')
            self.p3[i] = self.p3[i].replace('Na','NA')
            self.p4[i] = self.p4[i].replace('Na','NA')
            self.p5[i] = self.p5[i].replace('Na','NA')

            self.r1[i] = self.r1[i].replace('Cl','CL')
            self.r2[i] = self.r2[i].replace('Cl','CL')
            self.p1[i] = self.p1[i].replace('Cl','CL')
            self.p2[i] = self.p2[i].replace('Cl','CL')
            self.p3[i] = self.p3[i].replace('Cl','CL')
            self.p4[i] = self.p4[i].replace('Cl','CL')
            self.p5[i] = self.p5[i].replace('Cl','CL')

        TMPspecies = set( np.concatenate([self.r1,self.r2,self.p1,self.p2,
                                          self.p3,self.p4,self.p5],axis=0) )
        TMPspecies.discard('')
        TMPspecies.discard('           ')
        TMPspecies.discard('FREEZE')
        TMPspecies.discard('PHOTON')
        TMPspecies.discard('DESORB')
        TMPspecies.discard('CRPHOT')
        TMPspecies.discard('CRP')
        TMPspecies.discard('XRAYS')

        self.species = list( TMPspecies )
        self.species.sort()

        print 'Converting to ALCHEMIC nomenclature:'
        print '{species}* notation --> g{species} notation'
        print '{e-} notation --> {ELECTR} notation'
        print '{He,Fe,Mg,Si,Cl,Na} notation --> {HE,FE,MG,SI,CL,NA} notation'
        print 'Done!\n'

# ADD NETWORKS
#
# Create a network (r1, r2, p1...p4, alpha, beta, gamma) which combines the gas-
# phase and grain surface reactions.
#
    def __add__(self, other):

        if other.index[0] == 1:
           other.index = max(self.index) + other.index

        new_network = network(np.concatenate([self.index,other.index],axis=0),
                              np.concatenate([self.r1,other.r1],axis=0),
                              np.concatenate([self.r2,other.r2],axis=0),
                              np.concatenate([self.p1,other.p1],axis=0),
                              np.concatenate([self.p2,other.p2],axis=0),
                              np.concatenate([self.p3,other.p3],axis=0),
                              np.concatenate([self.p4,other.p4],axis=0),
                              np.concatenate([self.p5,other.p5],axis=0),
                              np.concatenate([self.alpha,other.alpha],axis=0),
                              np.concatenate([self.beta,other.beta],axis=0),
                              np.concatenate([self.gamma,other.gamma],axis=0),
                              np.concatenate([self.rtype,other.rtype],axis=0),
                              np.concatenate([self.formul,other.formul],axis=0),
                              self.comment + ' | ' + other.comment )

        return new_network

#
# SORTREACTANTS
#
# Sorts the reactant and product names to aphabetical order. The index and order
# of the reactions does not change, only the order of R1 and R2; and P1, P2, P3,
# P4 and P5, respectively.
# The main network does not change, but a new object with network class will be
# returned.

    def sortreactants(self):

        sortedntw = copy.deepcopy(self)

# Order the main network
        for i in range(len(sortedntw.r1)):
            reacTMP = [sortedntw.r1[i],sortedntw.r2[i]]
            reacTMP.sort(key=self.atomnumber, reverse=True)
            prodTMP = [sortedntw.p1[i],sortedntw.p2[i],sortedntw.p3[i],sortedntw.p4[i],sortedntw.p5[i]]
            prodTMP.sort(key=self.atomnumber, reverse=True)

            sortedntw.r1[i] = reacTMP[0]
            sortedntw.r2[i] = reacTMP[1]

            sortedntw.p1[i] = prodTMP[0]
            sortedntw.p2[i] = prodTMP[1]
            sortedntw.p3[i] = prodTMP[2]
            sortedntw.p4[i] = prodTMP[3]
            sortedntw.p5[i] = prodTMP[4]

        return sortedntw

#
# ATOMNUMBER
#
# This method takes a species name and searches the NETWORK object species list for
# the given name. If found it will return the atom number of the species.

    def atomnumber(self, SpecToFind):
        if SpecToFind in ["","PHOTON","XRAYS","CRPHOT","ELECTR","CRP","DESORB","FREEZE"]:
           atmnum = 0           # large value that this goes to the end of the list
        elif SpecToFind in ["G0", "G-", "G+"]:
           atmnum = 1
        else:
           try:
              index = self.species.index(SpecToFind)
              atmnum = self.anum[index]
              if ("+" in SpecToFind) or ("-" in SpecToFind):
                  atmnum = atmnum + 100
           except:
              print "Species", SpecToFind, "is not found in species list!"
              atmnum = -1000             # this will put the unknown species to the beginning

        return atmnum


#
# UPDATERATES
#
# Updates the gas phase rates using a second network. The second network is input
# and must have network class, as defind in this code.
# The reactants and the products for each reactions are ordered in alphabetic order,
# then they are compared to find the

    def updaterates(self, other, eps=0.01, addnew=True, updold=True):

        base = self.sortreactants()
        update = other.sortreactants()

        NreactNew = 0            # new
        NreactUpd = 0            # updated
        NreactNU =  0            # not unique

        NewReactions = []
        UpdatedReactions = []

        for i in range(len(update.r1)):
            r1find = exactmatch(base.r1, update.r1[i])
            r2find = exactmatch(base.r2, update.r2[i])

            p1find = exactmatch(base.p1, update.p1[i])
            p2find = exactmatch(base.p2, update.p2[i])
            p3find = exactmatch(base.p3, update.p3[i])
            p4find = exactmatch(base.p4, update.p4[i])
            p5find = exactmatch(base.p5, update.p5[i])

            loc = r1find + r2find + p1find + p2find + p3find + p4find + p5find



            loc2 = np.where(loc >= 0)

            if ( len(loc2[0]) > 1 ):
               print "Reaction is duplicate:"
               print update.r1[i], ' + ', update.r2[i], " -> ", update.p1[i], \
                     " + ", update.p2[i], " + ", update.p3[i], " + ", update.p4[i]
               print update.r1[i], update.r2[i], update.p1[i], update.p2[i]
               print base.r1[loc2], base.r2[loc2], base.p1[loc2], base.p2[loc2]
               print r1find[loc2], r2find[loc2], p1find[loc2], p2find[loc2]

               NreactNU = NreactNU + 1
            elif ( len(loc2[0]) == 1 ):
               dalpha = 0.0
               dbeta = 0.0
               dgamma = 0.0
               if update.alpha[i] != 0e0:
                  dalpha = abs(self.alpha[loc2]-update.alpha[i]) / update.alpha[i]
               if update.beta[i] != 0e0:
                  dbeta = abs(self.beta[loc2]-update.beta[i]) / update.beta[i]
               if update.gamma[i] != 0e0:
                  dgamma = abs(self.gamma[loc2]-update.gamma[i]) / update.gamma[i]

               if (dalpha > eps) or (dbeta > eps) or (dgamma > eps):

                  UpdatedReactions.append((update.r1[i], ' + ', update.r2[i], " -> ", update.p1[i], \
                       " + ", update.p2[i], "..."))
                  UpdatedReactions.append( ('Old:', str(self.alpha[loc2][0]), str(self.beta[loc2][0]), str(self.gamma[loc2][0])) )
                  UpdatedReactions.append( ('New:', str(update.alpha[i]), str(update.beta[i]), str(update.gamma[i])) )

                  if update:
                      self.alpha[loc2]  = update.alpha[i]
                      self.beta[loc2]   = update.beta[i]
                      self.gamma[loc2]  = update.gamma[i]
                      self.rtype[loc2]  = update.rtype[i]
                      self.formul[loc2] = update.formul[i]

                  NreactUpd = NreactUpd + 1
            else:
               NewReactions.append( (update.r1[i], ' + ', update.r2[i], " -> ", update.p1[i], \
                     " + ", update.p2[i], "...") )
               NreactNew = NreactNew + 1
               if updold:
                   self.r1 = add2nparr(self.r1,update.r1[i])
                   self.r2 = add2nparr(self.r2,update.r2[i])
                   self.p1 = add2nparr(self.p1,update.p1[i])
                   self.p2 = add2nparr(self.p2,update.p2[i])
                   self.p3 = add2nparr(self.p3,update.p3[i])
                   self.p4 = add2nparr(self.p4,update.p4[i])
                   self.p5 = add2nparr(self.p5,update.p5[i])
                   self.alpha = add2nparr(self.alpha,update.alpha[i])
                   self.beta = add2nparr(self.beta,update.beta[i])
                   self.gamma = add2nparr(self.gamma,update.gamma[i])
                   self.rtype = add2nparr(self.rtype,update.rtype[i])
                   self.formul = add2nparr(self.formul,update.formul[i])
                   index = self.index[-1] + 1
                   self.index = add2nparr(self.index,index)
                   # Missing species
                   self.AddSpecIfNotPresent(update, update.r1[i])
                   self.AddSpecIfNotPresent(update, update.r2[i])
                   self.AddSpecIfNotPresent(update, update.p1[i])
                   self.AddSpecIfNotPresent(update, update.p2[i])
                   self.AddSpecIfNotPresent(update, update.p3[i])
                   self.AddSpecIfNotPresent(update, update.p4[i])

        print 'Network is combined with', update.comment, '!\n'
        print "Number of reactions in both networks:   ", NreactUpd
        if updold:
            print "Their rates are updated!"
        else:
            print "Their rates are NOT updated!"
        print "-------------------------------------------"
        for i in UpdatedReactions:
            print '    '.join(i)
        print "                                            "
        print "Number of non-unique reactions:", NreactNU
        print "Number of new reactions       :", NreactNew
        if addnew:
            print "Which are added!"
        else:
            print "Which are NOT added!"
        print "-------------------------------------------"
        for i in NewReactions:
            print '    '.join(i)
        print "                                            "

    def AddSpecIfNotPresent(self, update, species):
        DiscardSpecList = ["CRP","CRPHOT","FREEZE","DESORB","XRAYS","PHOTON"]

        if (species != '') and (species not in self.species) and (species not in DiscardSpecList):
           print species, "is missing!"
           SpecNum = len(self.species)
           UpdateSpecLoc = update.species.index(species)
           self.species.append(update.species[UpdateSpecLoc])
           self.anum.resize(SpecNum + 1, refcheck=False)
           self.amass.resize(SpecNum + 1, refcheck=False)
           self.anum[SpecNum] = update.anum[UpdateSpecLoc]
           self.amass[SpecNum] = update.amass[UpdateSpecLoc]

#
# CHECK
#
# This method checks the network for self-consistency, mass and charge conservations.
# at the moment the test is only done for the atomic number conservation.

    def check(self):

        print "Checking for atomic number and charge conservation..."
        ErrorNum = 0

        for i in range(len(self.r1)):
            leftSide = self.atomnumber(self.r1[i]) + self.atomnumber(self.r2[i])
            leftCharge = chargenumber(self.r1[i]) + chargenumber(self.r2[i])
            if (leftSide <= -2000):
               leftSide + 2000
               print "leftSide << 2000!"
            if (leftSide <= -1000):
               print "leftSide << 1000!"
            if (leftSide >= 200):
               leftSide = leftSide - 200.
            if (leftSide >= 100):
               leftSide = leftSide - 100.
            rightSide = self.atomnumber(self.p1[i]) + self.atomnumber(self.p2[i]) + \
                        self.atomnumber(self.p3[i]) + self.atomnumber(self.p4[i]) + \
                        self.atomnumber(self.p5[i])
            rightCharge = chargenumber(self.p1[i]) + chargenumber(self.p2[i]) + \
                          chargenumber(self.p3[i]) + chargenumber(self.p4[i]) + \
                          chargenumber(self.p5[i])
            if (rightSide <= -2000):
#               leftSide + 2000
              print "leftSide << 2000!"
            if (rightSide <= -1000):
               print "leftSide << 1000!"
#            if (rightSide >= 500):
#               rightSide = rightSide - 500.
#            if (rightSide >= 400):
#               rightSide = rightSide - 400.
            if (rightSide >= 300):
               rightSide = rightSide - 300.
            if (rightSide >= 200):
               rightSide = rightSide - 200.
            if (rightSide >= 100):
               rightSide = rightSide - 100.

            if (leftSide != rightSide):
               print "Error atomic mass is not conserved!"
               print self.r1[i], "+", self.r2[i], "->", self.p1[i], "+", self.p2[i], "+", self.p3[i]
               print leftSide, "<->", rightSide
               ErrorNum = ErrorNum + 1
            if (leftCharge != rightCharge):
               print "Error charge is not conserved!"
               print self.r1[i], "+", self.r2[i], "->", self.p1[i], "+", self.p2[i], "+", self.p3[i]
               print leftCharge, "<->", rightCharge
               ErrorNum = ErrorNum + 1

        if ErrorNum !=0:
           print "Errors have been found!"
        else:
           print "Check complete: no errors!"

# WRITE2ALCHEMIC
#
# Write network in the ALCHEMIC input format
#
    def write2alchemic(self, networkfile='rreactions_op_Sipila2015.dat',
        specfile='rspecies_op_Sipila2015.dat'):

        if os.path.isfile( networkfile ):
           print 'File', networkfile, 'already exists!'
           what2do = raw_input('Do you want to overwrite? (y/n)')
           if what2do == 'n' or what2do == 'no':
              print 'No file was written!'
              return
        outfile = open(networkfile, 'w')

# Write the header
        nreact = len( self.index )
        outfile.write( ('%6u ' + self.comment + '\n') % nreact )

        for i in range( len( self.index) ):
            outfile.write( '%6u %-12s%-12s%-12s%-12s%-12s%-12s%-12s%-12s%9.2E%9.2E%9.2E% 6u %6u \n'
                    % (self.index[i],self.r1[i],self.r2[i],' ',self.p1[i],self.p2[i],self.p3[i],
                       self.p4[i],self.p5[i],self.alpha[i],self.beta[i],self.gamma[i],self.rtype[i],self.formul[i] ) )

        outfile.close()

        print 'File', networkfile, 'is written!'

# Write the species file
        if os.path.isfile( specfile ):
           print 'File', specfile, 'already exists!'
           what2do = raw_input('Do you want to overwrite? (y/n)')
           if what2do == 'n' or what2do == 'no':
              print 'No file was written!'
              return
        outfile = open(specfile, 'w')

        nspec = len( self.species )
        outfile.write( ('%6u ' + self.comment + '\n') % nspec )

        for i in range( nspec ):
            outfile.write( '%-12s\n' % (self.species[i]) )

        outfile.close()

        print 'File', specfile, 'is written!'

# COUNT_ATOMS
#
# For each species involved in the reaction, calculate the atom number.
# This is necessary when reducing the chemical network by molecule mass.
#
    def count_atoms( self ):

# Initialization of atom's names:
        Atoms1C = ['H','C','N','O','S','P','D']
        mass1C  = [1.00000E+00,1.20110E+01,1.40067E+01,1.59994E+01,3.20660E+01,
                   3.09738E+01,2.00000E+00]
        Atoms2C = ['HE','FE','SI','NA','MG','CL']
        mass2C  = [4.00260E-00,5.58470E+01,2.80855E+01,2.29898E+01,2.43050E+01,
                   3.54527E+01]

        for tmpSpec in self.species:
            ct = self.species.index(tmpSpec)  # what is the current index?
# Remove 'g' and ortho and para states and the mantel 'm' species?
            tmpSpec = tmpSpec.replace('g','')
            tmpSpec = tmpSpec.replace('o','')
            tmpSpec = tmpSpec.replace('p','')
            tmpSpec = tmpSpec.replace('m','')
# Remove '-' and '+' ionization states
            tmpSpec = tmpSpec.replace('-','')
            tmpSpec = tmpSpec.replace('+','')
# Take care of the PHOTONS and ELECTR
            if tmpSpec == 'PHOTON':
               tmpMass = 0.
               tmpSpec = ''
               tmpAtms = 0
            if tmpSpec == 'ELECTR':
               tmpMass = 5.4462409e-07
               tmpSpec = ''
               tmpAtms = 0
            if tmpSpec == 'CRPHOT':
               tmpMass = 0.
               tmpSpec = ''
               tmpAtms = 0

            tmpMass = 0.
            tmpAtms = 0

            for s2i in range( len(Atoms2C) ):
                while Atoms2C[s2i] in tmpSpec:
                     strindex = tmpSpec.find(Atoms2C[s2i])
                     if ( strindex >= 0 ):
                     # check if mulitple atoms?
                        multipl = 1.
                        ToRemove = 0
                        if strindex+2 <= len(tmpSpec)-1:
                           if tmpSpec[strindex+2].isdigit() :
                                multipl = float(tmpSpec[strindex+2])
                                ToRemove = 1
                     # check if 2 digits
                        if strindex+3 <= len(tmpSpec)-1:
                           if tmpSpec[strindex+2:strindex+3].isdigit() :
                                multipl = float(tmpSpec[strindex+2:strindex+3])
                                ToRemove = 2
                        if ToRemove > 0:
                           tmpSpec = tmpSpec[:strindex+2] + tmpSpec[(strindex+2+ToRemove):]
                     # add up the mass, remove the atom from the string
                        tmpMass = tmpMass + ( multipl * mass2C[s2i] )
                        tmpSpec = tmpSpec[:strindex] + tmpSpec[(strindex+2):]
                        tmpAtms = tmpAtms + multipl

            for s1i in range( len(Atoms1C) ):
                while Atoms1C[s1i] in tmpSpec:
                     strindex = tmpSpec.find(Atoms1C[s1i])
                     if ( strindex >= 0 ):
                     # check if mulitple atoms?
                        multipl = 1.
                        ToRemove = 0
                        if strindex+1 <= len(tmpSpec)-1:
                           if tmpSpec[strindex+1].isdigit() :
                                multipl = float(tmpSpec[strindex+1])
                                ToRemove = 1
                     # check if 2 digits
                        if strindex+2 <= len(tmpSpec)-1:
                           if tmpSpec[strindex+1:strindex+2+1].isdigit() :
                                multipl = float(tmpSpec[strindex+1:strindex+1+2])
                                ToRemove = 2
                        if ToRemove > 0:
                           tmpSpec = tmpSpec[:strindex+1] + tmpSpec[(strindex+1+ToRemove):]

                     # add up the mass
                        tmpMass = tmpMass + ( multipl * mass1C[s1i] )
                        tmpSpec = tmpSpec[:strindex] + tmpSpec[(strindex+1):]
                        tmpAtms = tmpAtms + multipl

            self.amass[ct] = tmpMass
            self.anum[ct] = tmpAtms

        print 'Counting mass and atom number is done!\n'
#
# READ_NETWORK
#
# Function to read the network files supplied by Oli (seperate gas-phase and
# grain surface reaction files, the accretion and dessorption to and from grains
# is not included in the files).
#
# In future: to be generalized for ALCHEMIC networks too!
#
def read_network(input_file='ir.15_grain', comment='This is Oli\'s grain network', skiplines=0):
# Define the variable we want to use outside of the function

# Get the number of lines for array creation
     with open(input_file) as network_file:
          nlines = len(network_file.readlines()) - 1 - skiplines

# Create the arrays:
     index  = np.zeros(nlines,dtype=np.int32)
     rtype  = np.zeros(nlines,dtype=np.int32)
     formul = np.zeros(nlines,dtype=np.int32)

     r1 = np.chararray(nlines,11)
     r2 = np.chararray(nlines,11)
     p1 = np.chararray(nlines,11)
     p2 = np.chararray(nlines,11)
     p3 = np.chararray(nlines,11)
     p4 = np.chararray(nlines,11)
     p5 = np.chararray(nlines,11)

     alpha = np.zeros(nlines,dtype=np.float64)
     beta  = np.zeros(nlines,dtype=np.float64)
     gamma = np.zeros(nlines,dtype=np.float64)

     dumy1 = np.zeros(nlines,dtype=np.int8)
     dumy2 = np.zeros(nlines,dtype=np.int8)

# Read the data by lines and split the lines by character spacing

     f = open(input_file, 'r')
     linecounter = 0
     if input_file[0:4] == 'kida':
        comment='This is KIDA network'
        for line in f:
            if line[0] != '!':
               r1[linecounter] = line[0:10]
               r2[linecounter] = line[10:20]
               p1[linecounter] = line[33:44]
               p2[linecounter] = line[44:55]
               p3[linecounter] = line[55:66]
               p4[linecounter] = line[66:77]
               p5[linecounter] = ''

               alpha[linecounter] = line[91:100]
               beta[linecounter] = line[102:111]
               gamma[linecounter] = line[113:122]

               index[linecounter]  = linecounter+1  #line[166:171]
               rtype[linecounter]  = line[146:148]
               formul[linecounter] = line[163:165]

               linecounter = linecounter + 1
     elif input_file[0:5] == 'grain':
        comment='This is grain surface ntw'
        for line in f:
            if line[0] != '!':
               r1[linecounter] = line[7:18]
               r2[linecounter] = line[19:30]
               p1[linecounter] = line[43:54]
               p2[linecounter] = line[55:66]
               p3[linecounter] = line[67:78]
               p4[linecounter] = line[79:90]
               p5[linecounter] = ''

               alpha[linecounter] = line[104:112]
               beta[linecounter] = line[112:121]
               gamma[linecounter] = line[121:130]

               index[linecounter]  = linecounter+1  #line[0:6]
               rtype[linecounter]  = line[130:133]
               formul[linecounter] = 3

               linecounter = linecounter + 1
     elif input_file[0:3] == 'osu':
        comment='This is OSU network'
        for line in f:
            if line[0] != '!':
               r1[linecounter] = line[0:8]
               r2[linecounter] = line[8:24]
               p1[linecounter] = line[24:32]
               p2[linecounter] = line[32:40]
               p3[linecounter] = line[40:48]
               p4[linecounter] = line[48:56]
               p5[linecounter] = ''

               alpha[linecounter] = line[64:73]
               beta[linecounter]  = line[73:82]
               gamma[linecounter] = line[82:91]

               index[linecounter]  = linecounter+1  #line[0:6]
               rtype[linecounter]  = line[91:93]
               formul[linecounter] = 3

               linecounter = linecounter + 1
     elif input_file[0:6] == 'update':
        comment='Sipila2015 updates'
        for line in f:
            if line[0] != '!':
               r1[linecounter] = line[0:12]
               r2[linecounter] = line[12:23]
               p1[linecounter] = line[23:36]
               p2[linecounter] = line[36:48]
               p3[linecounter] = line[48:58]
               p4[linecounter] = ''
               p5[linecounter] = ''

               alpha[linecounter] = line[58:68]
               beta[linecounter]  = line[68:74]
               gamma[linecounter] = line[74:79]

               index[linecounter]  = linecounter+1  #line[0:6]
               rtype[linecounter]  = -1 #line[91:93]
               formul[linecounter] = 3

               if r2[linecounter] == "CRPHOT":
                  alpha[linecounter] = alpha[linecounter] / 1.3E-17

               linecounter = linecounter + 1
     elif input_file[0:7] == 'OSU06XR':
        comment='This is OSU updated of Semenov+ (2010)'
        for line in f:
            if line[0] != '!':
               r1[linecounter] = line[7:19]
               r2[linecounter] = line[19:43]
               p1[linecounter] = line[43:55]
               p2[linecounter] = line[55:67]
               p3[linecounter] = line[67:79]
               p4[linecounter] = line[79:91]
               p5[linecounter] = line[91:103]

               alpha[linecounter] = line[103:112]
               beta[linecounter]  = line[112:121]
               gamma[linecounter] = line[121:130]

               index[linecounter]  = linecounter+1
               rtype[linecounter]  = line[130:133]
               formul[linecounter] = 3

               linecounter = linecounter + 1
     else:
        for line in f:
            if line[0] != '!':
               index[linecounter] = line[0:5]
               r1[linecounter] = line[5:17]
               r2[linecounter] = line[17:28]
               p1[linecounter] = line[28:39]
               p2[linecounter] = line[39:50]
               p3[linecounter] = line[50:61]
               p4[linecounter] = line[61:72]
               p5[linecounter] = ''

               alpha[linecounter] = line[72:80]
               beta[linecounter] = line[80:90]
               gamma[linecounter] = line[90:100]

               formul[linecounter] = 3

               linecounter = linecounter + 1

     f.close()

     print 'Reading file', input_file, 'is done!\n'

# Create and return the network

     return network(index, r1, r2, p1, p2, p3, p4, p5, alpha, beta, gamma, rtype, formul, comment)

#
# READ_EDES
#
# Function to read the desoroption energy file supplied by Olli.
# First the species names and dessorption energies are read. Then a network is
# constructed in similar fasion as the main network. This can be added to other
# networks by the addition operator.
#
def read_edes(GasNet, adsorp_file = 'adsorp.energy', acten_file = 'active.energy', grain_file = 'garrod2008_network.dat', comment = 'This is Olli\'s desorbtion energies', AddAll=False):
# Get the number of lines for array creation
    print "Reading binding energies from", adsorp_file, "..."
    with open(adsorp_file) as edes_file:
        nlines = len(edes_file.readlines())

# Create the arrays:
    index = np.zeros(nlines,dtype=np.int32)
    SpecEd = []
    edes = np.zeros(nlines,dtype=np.float64)

# Read the data by lines and split the lines by character spacing
    f = open(adsorp_file, 'r')
    linecounter = 0
    for line in f:
        if line[0] != '#' and line[0] != ' ' and line[0] != '\n':
            index[linecounter] = linecounter
            tmp = line.split()
            Stmp = tmp[0].replace('o','').replace('p','')
            Stmp = Stmp.replace('Mg','MG').replace('He','HE').replace('Fe','FE')
            Stmp = Stmp.replace('Si','SI').replace('Na','NA').replace('Cl','CL')
            SpecEd.append(Stmp.strip())
            edes[linecounter] = float(tmp[1])
            linecounter = linecounter + 1
    f.close()
    edes = edes[0:linecounter]

# Get the list of neutral species in the gas-phase network
# and try to find them in the SpecEd list
    SpecFound = []
    Eds = []
    for s in GasNet.species:
        if ( chargenumber(s) == 0 ):
            try:
                iEds = SpecEd.index(s)
                SpecFound.append(s)
                Eds.append(edes[iEds])
            except:
                print s, 'not found!'
    nreact = len(SpecFound)
    index  = np.zeros(2*nreact,dtype=np.int32)
    rtype  = np.zeros(2*nreact,dtype=np.int32)
    formul = np.zeros(2*nreact,dtype=np.int32)

    r1 = np.chararray(2*nreact,11)
    r1[:] = ''
    r2 = np.chararray(2*nreact,11)
    r2[:] = ''
    p1 = np.chararray(2*nreact,11)
    p1[:] = ''
    p2 = np.chararray(2*nreact,11)
    p2[:] = ''
    p3 = np.chararray(2*nreact,11)
    p3[:] = ''
    p4 = np.chararray(2*nreact,11)
    p4[:] = ''
    p5 = np.chararray(2*nreact,11)
    p5[:] = ''

    alpha = np.zeros(2*nreact,dtype=np.float64)
    beta  = np.zeros(2*nreact,dtype=np.float64)
    gamma = np.zeros(2*nreact,dtype=np.float64)

    for cc in range(nreact):
        # Freeze out reaction
        r1[cc] = SpecFound[cc]
        r2[cc] = 'FREEZE'
        p1[cc] = 'g'+SpecFound[cc]
        alpha[cc] = 1.0
        # Desorbtion reaction
        jj = cc+nreact
        r1[jj] = 'g'+SpecFound[cc]
        r2[jj] = 'DESORB'
        p1[jj] = SpecFound[cc]
        alpha[jj] = 1.0
        gamma[jj] = Eds[cc]
        # Metadata
        index[cc] = cc+1
        index[jj] = jj+1
        rtype[cc] = 15
        rtype[jj] = 66
        formul[cc] = 3
        formul[jj] = 3

# Copy the photo- and CRP induced reactions from the gas phase network.
    print "Copying gas phase photo processes to grain surface species..."
    PhotoList = ["PHOTON","CRPHOT"]
    for cc in range(len(GasNet.index)):
        if (GasNet.r1[cc] in SpecFound) and (GasNet.r2[cc] in PhotoList) and (chargenumber(GasNet.p1[cc]) == 0) and (chargenumber(GasNet.p2[cc]) == 0):
            r1 = add2nparr(r1, 'g'+GasNet.r1[cc])
            r2 = add2nparr(r2, GasNet.r2[cc])
            p1 = add2nparr(p1,'g'+GasNet.p1[cc])
            if GasNet.p2[cc] == "":
                p2 = add2nparr(p2, GasNet.p2[cc])
            else:
                p2 = add2nparr(p2, 'g'+GasNet.p2[cc])
            if GasNet.p3[cc] == "":
                p3 = add2nparr(p3, GasNet.p3[cc])
            else:
                p3 = add2nparr(p3, 'g'+GasNet.p3[cc])
            if GasNet.p4[cc] == "":
                p4 = add2nparr(p4, GasNet.p4[cc])
            else:
                p4 = add2nparr(p4, 'g'+GasNet.p4[cc])
            if GasNet.p5[cc] == "":
                p5 = add2nparr(p5, GasNet.p5[cc])
            else:
                p5 = add2nparr(p5, 'g'+GasNet.p5[cc])

            alpha = add2nparr(alpha, GasNet.alpha[cc])
            beta = add2nparr(beta, GasNet.beta[cc])
            gamma = add2nparr(gamma, GasNet.gamma[cc])
            rtype = add2nparr(rtype, GasNet.rtype[cc])
            formul = add2nparr(formul, GasNet.formul[cc])
            index = add2nparr(index, index[-1] + 1)

# Read the activation energy file (Garrod 2008)
    print "Reading reaction activation energies from", acten_file, "..."
    f = open(acten_file, 'r')
    linecounter = 0
    ActEn_index = []
    ActEn_r1 = []
    ActEn_r2 = []
    ActEn_p1 = []
    ActEn_p2 = []
    ActEn_p3 = []
    ActEn_p4 = []
    ActEn_react = []
    ActEn = []
    BarWidth = []
    for line in f:
        if line[0] != '#' and line[0] != ' ' and line[0] != '\n':
            ActEn_index.append(linecounter+1)
            ActEn_r1.append(line[0:8].strip())
            ActEn_r2.append(line[8:16].strip())
            ActEn_p1.append(line[16:24].strip())
            ActEn_p2.append(line[24:32].strip())
            ActEn_p3.append(line[32:40].strip())
            ActEn_p4.append(line[40:48].strip())
            ActEn_react.append(line[0:8].strip()+'+'+line[8:16].strip()+'->'+line[16:24].strip())
            ActEn.append(float(line[48:57]))
            BarWidth.append(float(line[58:63]))
            linecounter = linecounter + 1
    f.close()

# Read the grain surface chemistry file (curretly Garrod 2008)
    print "Reading surface reaction network", grain_file, "..."
    if grain_file == "garrod2008_network.dat":
        rformat = 1
    elif grain_file == "grain_surf_dima.dat":
        rformat = 2
    elif grain_file == "ir.15_grain_corrected_clean":
        rformat = 3
    else:
        print "Grain file not known, stopping!"
        return -1
    f = open(grain_file, 'r')
    rcounter = 0
    for line in f:
        if line[0] != '#' and line[0] != ' ' and line[0] != '\n':
            if (rformat == 1):
                GNtw_r1 = line[0:8].strip()
                GNtw_r2 = line[8:18].strip()
                GNtw_p1 = line[24:32].strip()
                GNtw_p2 = line[32:40].strip()
                GNtw_p3 = line[40:48].strip()
                GNtw_p4 = line[48:56].strip()
                GNtw_p5 = line[56:64].strip()
                GNtw_alpha = float(line[64:73])
                if (GNtw_r1.replace("g","") in SpecFound) and (GNtw_r2.replace("g","") in SpecFound) and (GNtw_p1.replace("g","") in SpecFound):
                    if (GNtw_p2 == '') or (GNtw_p2.replace("g","") in SpecFound):
                        r1 = add2nparr(r1, GNtw_r1)
                        r2 = add2nparr(r2, GNtw_r2)
                        p1 = add2nparr(p1, GNtw_p1)
                        p2 = add2nparr(p2, GNtw_p2)
                        p3 = add2nparr(p3, GNtw_p3)
                        p4 = add2nparr(p4, GNtw_p4)
                        p5 = add2nparr(p5, GNtw_p5)

                        alpha = add2nparr(alpha, GNtw_alpha)
                        beta = add2nparr(beta, 0.0)
                        # Find the activation energy if exists
                        try:
                            jj = ActEn_react.index(GNtw_r1+'+'+GNtw_r2+'->'+GNtw_p1)
                            ActivationEnergy = ActEn[jj]
                        except:
                            ActivationEnergy = 0.0
                        gamma = add2nparr(gamma, ActivationEnergy)

                        rtype = add2nparr(rtype, 14)
                        formul = add2nparr(formul, 3)
                        index = add2nparr(index, index[-1] + 1)
                        rcounter = rcounter + 1
            elif (rformat == 2):
                GNtw_r1 = line[5:17].strip()
                GNtw_r2 = line[17:36].strip()
                GNtw_p1 = line[41:53].strip()
                GNtw_p2 = line[53:65].strip()
                GNtw_p3 = line[65:77].strip()
                GNtw_p4 = line[77:89].strip()
                GNtw_p5 = line[89:100].strip()

                GNtw_alpha = float(line[101:110])
                GNtw_beta = float(line[110:119])
                GNtw_gamma = float(line[119:128])

                if (GNtw_r1.replace("g","") in SpecFound) and (GNtw_r2.replace("g","") in SpecFound) and (GNtw_p1.replace("g","") in SpecFound) and ("g" in GNtw_p1):
                    r1 = add2nparr(r1, GNtw_r1)
                    r2 = add2nparr(r2, GNtw_r2)
                    p1 = add2nparr(p1, GNtw_p1)
                    p2 = add2nparr(p2, GNtw_p2)
                    p3 = add2nparr(p3, GNtw_p3)
                    p4 = add2nparr(p4, GNtw_p4)
                    p5 = add2nparr(p5, GNtw_p5)

                    alpha = add2nparr(alpha, GNtw_alpha)
                    beta = add2nparr(beta, 0.0)
                    gamma = add2nparr(gamma, GNtw_beta)

                    rtype = add2nparr(rtype, 14)
                    formul = add2nparr(formul, 3)
                    index = add2nparr(index, index[-1] + 1)
                    rcounter = rcounter + 1
                    try:
                        jj = ActEn_react.index(GNtw_r1+'+'+GNtw_r2+'->'+GNtw_p1)
                        if (GNtw_gamma != ActEn[jj]) and (GNtw_gamma != 0.):
                            print ActEn_react[jj], ActEn[jj], GNtw_gamma
                    except:
                        ActivationEnergy = 0.0
            elif (rformat == 3):
                GNtw_r1 = line[4:15].strip()
                if "*" in GNtw_r1:
                    GNtw_r1 = 'g'+GNtw_r1.replace("*","")
                GNtw_r2 = line[15:26].strip()
                if "*" in GNtw_r2:
                    GNtw_r2 = 'g'+GNtw_r2.replace("*","")
                GNtw_p1 = line[26:37].strip()
                if "*" in GNtw_p1:
                    GNtw_p1 = 'g'+GNtw_p1.replace("*","")
                GNtw_p2 = line[37:48].strip()
                if "*" in GNtw_p2:
                    GNtw_p2 = 'g'+GNtw_p2.replace("*","")
                GNtw_p3 = line[48:59].strip()
                if "*" in GNtw_p3:
                    GNtw_p3 = 'g'+GNtw_p3.replace("*","")
                GNtw_p4 = ""
                GNtw_p5 = ""

                GNtw_alpha = float(line[69:78])
                GNtw_beta = float(line[79:88])
                GNtw_gamma = float(line[89:98])

                if ((GNtw_r1.replace("g","") in SpecFound) and (GNtw_r2.replace("g","") in SpecFound) and (GNtw_p1.replace("g","") in SpecFound) and ("g" in GNtw_p1)) or AddAll:
                    r1 = add2nparr(r1, GNtw_r1)
                    r2 = add2nparr(r2, GNtw_r2)
                    p1 = add2nparr(p1, GNtw_p1)
                    p2 = add2nparr(p2, GNtw_p2)
                    p3 = add2nparr(p3, GNtw_p3)
                    p4 = add2nparr(p4, GNtw_p4)
                    p5 = add2nparr(p5, GNtw_p5)

                    alpha = add2nparr(alpha, GNtw_alpha)
                    beta = add2nparr(beta, 0.0)
                    gamma = add2nparr(gamma, GNtw_beta)

                    rtype = add2nparr(rtype, 14)
                    formul = add2nparr(formul, 3)
                    index = add2nparr(index, index[-1] + 1)
                    rcounter = rcounter + 1
                    try:
                        jj = ActEn_react.index(GNtw_r1+'+'+GNtw_r2+'->'+GNtw_p1)
                        if (GNtw_beta != ActEn[jj]) and (GNtw_beta != 0.):
                            print ActEn_react[jj], ActEn[jj], GNtw_beta
                    except:
                        ActivationEnergy = 0.0
                else:
                    if (GNtw_r1.replace("g","") not in SpecFound):
                        print GNtw_r1
                    if (GNtw_r2.replace("g","") not in SpecFound):
                        print GNtw_r2
                    if (GNtw_p1.replace("g","") not in SpecFound):
                        print GNtw_p1
    f.close()

    print rcounter, "grain surface reactions are found and added!"

    # Check if the reactants are in the neutral species list!

    return network(index, r1, r2, p1, p2, p3, p4, p5, alpha, beta, gamma, rtype, formul, comment)
#
# CHARGENUMBER
#

def chargenumber(SpecToFind):

     chargeplus = 0
     chargeminus = 0
     if ("++" in SpecToFind):
        chargeplus = chargeplus + 2
     elif ("+" in SpecToFind):
        chargeplus = chargeplus + 1
     if ("--" in SpecToFind):
        chargeminus = chargeminus + 2
     elif ("-" in SpecToFind) or ("ELECTR" in SpecToFind):
        chargeminus = chargeminus + 1

     return chargeplus - chargeminus

def exactmatch(chararray, char):

    indicator = np.zeros(len(chararray),dtype=np.int32) - 1  # initialize match indicator array

    for i in range(len(chararray)):
        if (chararray[i] == char):
           indicator[i] = 0

    return indicator

# READ_ALCHEMIC
#
# Reads the ALCHEMIC .idl output file to an object this can then be analysed and
# plotted. The code works similarly to its predeceser IDL code read_alchemic.pro
def read_alchemic(ChemFile="hwcloud_00215090.idl",perline=9):
    print "Reading " + ChemFile + "..."
# Read the data from the file
    f = open(ChemFile,"r")
    f.readline()
    specfile = f.readline().replace("\n","").strip()
    ratefile = f.readline().replace("\n","").strip()
    ipon = int(f.readline().replace("\n","").strip())
    ID = int(f.readline().replace("\n","").strip())
    x = float(f.readline().replace("\n","").strip())
    y = float(f.readline().replace("\n","").strip())
    z = float(f.readline().replace("\n","").strip())
    rhod = float(f.readline().replace("\n","").strip())
    mdmg = float(f.readline().replace("\n","").strip())
    grain = float(f.readline().replace("\n","").strip())
    albedo_UV = float(f.readline().replace("\n","").strip())
    tlast = float(f.readline().replace("\n","").strip())
    tfirst = float(f.readline().replace("\n","").strip())
    nstep = int(f.readline().replace("\n","").strip())

    time = readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
    Tg = readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
    Td = readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
    rho = readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
    AvSt = readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
    AvIS = readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
    G0 = readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
    fH2_St = readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
    fCO_St = readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
    fH2_IS = readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
    fCO_IS = readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
    ZetaCR = readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
    ZetaX = readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
    gdens = readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
    amu = readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)

    nInitSpec = int(f.readline().replace("\n","").strip())
    InitSpec = []
    InitAbuns = np.zeros(nInitSpec)
    for i in range(nInitSpec):
        line = (f.readline()).split()
        InitSpec.append(line[0].strip())
        InitAbuns[i] = float(line[1].replace("D","E"))

    ns = int(f.readline().replace("\n","").strip())
    spec = readIDLlikeArrayFromFile(f,nx=ns,dtype=np.str_,perline=9)

    nre = int(f.readline().replace("\n","").strip())
    r1 = readIDLlikeArrayFromFile(f,nx=nre,dtype=np.str_,perline=perline)
    r2 = readIDLlikeArrayFromFile(f,nx=nre,dtype=np.str_,perline=perline)
    p1 = readIDLlikeArrayFromFile(f,nx=nre,dtype=np.str_,perline=perline)
    p2 = readIDLlikeArrayFromFile(f,nx=nre,dtype=np.str_,perline=perline)
    p3 = readIDLlikeArrayFromFile(f,nx=nre,dtype=np.str_,perline=perline)
    p4 = readIDLlikeArrayFromFile(f,nx=nre,dtype=np.str_,perline=perline)
    p5 = readIDLlikeArrayFromFile(f,nx=nre,dtype=np.str_,perline=perline)

    ak = readIDLlikeArrayFromFile(f,nx=nre,ny=nstep,dtype=np.double)
    abuns = readIDLlikeArrayFromFile(f,nx=ns,ny=nstep,dtype=np.double)

    args = {'x':x,'y':y,'z':z,'rhod':rhod,'mdmg':mdmg,'grain':grain,'albedo':albedo_UV,
                    'time':time,'Tg':Tg,'Td':Td,'rho':rho,'AvSt':AvSt,'AvIS':AvIS,'G0':G0,
                    'fH2_St':fH2_St,'fH2_IS':fH2_IS,'fCO_IS':fCO_IS,'fCO_St':fCO_St,
                    'ZetaCR':ZetaCR,'ZetaX':ZetaX,'gdens':gdens,'amu':amu,'spec':spec,
                    'r1':r1,'r2':r2,'p1':p1,'p2':p2,'p3':p3,'p4':p4,'p5':p5,'ak':ak,
                    'abuns':abuns,'InitSpec':InitSpec,'InitAbuns':InitAbuns,
                    'specfile':specfile,'ratefile':ratefile}
    model = ChemModl(**args)
    return model

def read_sipila2015(ChemFile='Sipila2015_fig1.dat',nt=100,nm=12):
    print "Reading " + ChemFile + "..."
    # Read the data from the file
    mp = 1.6726000e-24    # proton mass in grams, this should be a global variable later
    year = 31557600.0     # year in seconds
    f = open(ChemFile,"r")
    # Read and work with the model physical properties
    for i in range(nm):
        line = f.readline().replace("#","").split()
        try:
            exec("%s = %d" % (line[0],float(line[1])))
        except:
            continue
    rho = n_H * (1.4*mp)
    f.readline()  # read the separating character
    # Read and work with the species names
    spec = f.readline().replace("#","").split()
    for i in range(len(spec)):
        spec[i] = spec[i].replace('Mg','MG')
        spec[i] = spec[i].replace('He','HE')
        spec[i] = spec[i].replace('Fe','FE')
        spec[i] = spec[i].replace('Si','SI')
        spec[i] = spec[i].replace('Na','NA')
        spec[i] = spec[i].replace('Cl','CL')
        spec[i] = spec[i].replace('g','G')
        spec[i] = spec[i].replace('e-','ELECTR')
        if ( '*' in spec[i] ):
            spec[i] = 'g' + spec[i].replace('*','')
    nspec = len(spec)
    # Read the abundances now (actually these are number densities)
    time = np.zeros(nt)
    abuns = np.zeros((nspec,nt))
    for i in range(nt):
        line = f.readline().split()
        time[i] = float(line[0])
        abuns[:,i] = np.array(line[1:],dtype=np.double) / n_H
    f.close()
    # Collect the data to a dictionary to be passed to the class creator
    args = {'x':0,'y':0,'z':0,'grain':a_g,'time':time/year,'Tg':float(T_gas),'Td':float(T_dust),
                'rho':rho,'AvIS':float(A_V),'G0':1.7,'ZetaCR':float(CRP),'gdens':float(n_H),
                'spec':spec,'abuns':abuns,}
    model = ChemModl(**args)
    return model

def read_semenov2010():

    print 'In construction!'

# readIDLlikeArrayFromFile
#
# This reads a chunk from an ascii text file to a numpy array.
def readIDLlikeArrayFromFile(file, nx=0, ny=1, perline=10, dtype=np.double):
# find out how many lines needs to be read to have the requested array length.
# This is controlled by the perline variable. This might not be always 10!
    nlines = (nx*ny) / perline
    reminder = (nx*ny) % perline
    if reminder > 0:
        nlines = nlines + 1
# The data will be stored in the StrTmp string, by adding up all the read
# lines.
#    StrTmp = file.readline()
    StrList = []
# Start reading in the matrix (array) line by line (j index stands for line,
# i index for column)
    for i in range(nlines):
        StrList.append(file.readline())

    StrTmp = " ".join(StrList)
#        if (StrTmp2 == ""):
#           print "Warning! Reading longer than the file's end."
    StrTmp = StrTmp.replace("\n"," ")
    if (dtype == np.double) or (dtype == np.float):
       StrTmp = StrTmp.replace("D","E")
       array = np.array(StrTmp.split(),dtype=dtype)
    else:
       chunks, chunk_size = len(StrTmp), len(StrTmp)/(nx-1)
       array = [ StrTmp[i:i+chunk_size] for i in range(0, chunks, chunk_size) ]
       for i in range(len(array)):
           array[i] = array[i].strip()
#               array = np.array(array,dtype=dtype)
    if (ny > 1):
        array = array.reshape(nx,ny)
    return array
# add2nparr
#
# This is an utility function to add new elements to a numpy array. This
# involves resizing the array and assigning the new elements with the given
# value. This is done extensively here, thus I add this small function.
def add2nparr(array, newelement):
    if "numpy" in str(type(array)):
        CurrentLength = len(array)
        array.resize(CurrentLength + 1, refcheck=False)
        array[CurrentLength] = newelement
        return array
    else:
        print "Error in ADD2NPARR(): not numpy array, type:", type(array)
        sys.exit()

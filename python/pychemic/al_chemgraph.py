"""
This module uses PyGraphViz to visualise subsets of a chemical reaction network.

The module is part of the PyChemic package and works with ALCHEMIC chemical
models. The goal of the module is to plot the most prominent reactions that
determine the abundance of selected target species on a network diagram. The
species and reactions are represented by circles. The reactant species and the
reaction products are connected to the reactions with edges (arrows). The
abundance of a species and the rate of a reaction are indicated by the size of
their representative circles on the diagram.

Examples:

1) To plot the chemical network of CO and O at the beginning of the simulation
for all reactions that contribute more than 1 per cent of the total destruction
and production rates of the species.::

    import pychemic as ntw
    chmodel = ntw.read_alchemic('osu09_gr5_00100000.idl')
    ntw.draw_chem_graph(chmodel, species=['CO','O'], ntop=8, nt=10, depth=1,
                    threshold=0.01)

.. image:: _resources/graph_example_1.png

2) To plot the chemical network of CO at the end of the simulation with a depth
of 3. Meaning that the reactions of the species determining the reactions of
species directly determining the abundace of CO will be plotted. This usually
leads to crowded graphs, hindering their usability.::

    import pychemic as ntw
    chmodel = ntw.read_alchemic('osu09_gr5_00100000.idl')
    ntw.draw_chem_graph(chmodel, species='CO', ntop=4, nt=99, depth=3,
                    threshold=0.1)

.. image:: _resources/graph_example_2.png
"""

import pygraphviz as pgv
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.image as mpimg
import numpy as np
import os
import al_commons   # from PyChemic

def add2graph(G, model, SpecList, RName=[], RDetail=[],
              nt=100, ntop=2, thrsh=0.01):
    """ Adds the reactions involving SpecisList to graph G as nodes and edges.

    The function first orders the production and destruction reactions based on
    their contribution to the production/destruction rate of target species.
    Selection criteria are applied to filter out the low rate reactions. The
    remaining reactions and the involved species are added as nodes and edges
    to the G graph.

    The G graph, a list of species (SpecList) required for the chemistry of the
    target species, the reaction names (i.e. k* identifiers, RName) and the
    reaction details (RDetail) are returned.

    :param G: graph created with the pygraphviz module
    :type G: pygraphviz.graph
    :param model: chemical model read in by PyChemic
    :type model: pychemic.ChemModl
    :param speclist: list of species whose reactions will be added to G
    :type speclist: str or list
    :param RName: reaction names that are added to the graph
    :type RName: srt list
    :param RDetail: reaction string with reactants, products and the rate
    :type RDetail: str list
    :param nt: time step of the simulation to be plotted (default 100)
    :type nt: int
    :param ntop: number of important destruction and production reactions to be
            added to G
    :type ntop: int
    :param thrsh: reactions contributing more than thrsh are used (default 0.01)
    :type thrsh: double or float

    :returns: Graph updated with the reactions invovling the specified species.
            Besides, further data is returned to be used to add mode species or
            when the recursive mode is chosen:

            - **NewSpecList** (*list*) -- list of species added to the graph
            - **RName** (*list*) -- updated list of reaction names in the graph
            - **RDetail** (*list*) -- updated list of reactions in the graph
    """
    if type(SpecList) == str:
        SpecList = [SpecList]
    NewSpecList = []
    NewSpecAbuns = []
    for cc in SpecList:
        if cc in model.spec:
            rc = len(RName) + 1
            icc = model.spec.index(cc)
            # Local copies of the reaction database for the selected species
            # and time step:
            ProdRate = model.pdrates[icc].ProdRate[:,nt]
            ProdRateCoef = model.pdrates[icc].ProdRateCoef[:,nt]
            ProdRateTot = model.pdrates[icc].ProdRateTot[nt]
            ProdR1 = model.pdrates[icc].ProdR1
            ProdR2 = model.pdrates[icc].ProdR2
            ProdP1 = model.pdrates[icc].ProdP1
            ProdP2 = model.pdrates[icc].ProdP2
            ProdP3 = model.pdrates[icc].ProdP3
            ProdP4 = model.pdrates[icc].ProdP4
            # Reorder the production and destruction reactions to represent the
            # current timestep:
            ProdWeight = ProdRate / ProdRateTot
            OrderIndexPr = ProdWeight.argsort()
            OrderIndexPr = OrderIndexPr[::-1]
            OrderIndexPr = np.extract(ProdWeight[OrderIndexPr] >= thrsh,
                                      OrderIndexPr)
            ProdRate = ProdRate[OrderIndexPr]
            ProdRateCoef = ProdRateCoef[OrderIndexPr] # Not used at the moment!
            ProdWeight = ProdWeight[OrderIndexPr]
            ProdR1 = al_commons.sortbynparr(ProdR1, OrderIndexPr)
            ProdR2 = al_commons.sortbynparr(ProdR2, OrderIndexPr)
            ProdP1 = al_commons.sortbynparr(ProdP1, OrderIndexPr)
            ProdP2 = al_commons.sortbynparr(ProdP2, OrderIndexPr)
            ProdP3 = al_commons.sortbynparr(ProdP3, OrderIndexPr)
            ProdP4 = al_commons.sortbynparr(ProdP4, OrderIndexPr)
            nProd = len(ProdWeight)
            # Start with the production reactions by adding them to the graph:
            for i in range(min([nProd,ntop])):
                rweight = ProdWeight[i]
                NewSpecList.append(ProdR1[i])
                NewSpecList.append(ProdR2[i])
                NewSpecList.append(ProdP1[i])
                NewSpecList.append(ProdP2[i])
                NewSpecList.append(ProdP3[i])
                NewSpecList.append(ProdP4[i])
                krTmp = "k" + str(rc)
                TxtTmp = "{0:6s} + {1:6s} -> {2:6s} + {3:6s}   {4:8.2E}".format(
                          ProdR1[i], ProdR2[i], ProdP1[i], ProdP2[i],
                          ProdRate[i])
                if TxtTmp not in RDetail:
                    RName.append(krTmp)
                    RDetail.append(TxtTmp)
                    G.add_node(RName[rc-1], type='reaction', color='blue',
                               weight=rweight)
                    abuns = model.abuns[model.spec.index(ProdR1[i]),nt]
                    G.add_node(ProdR1[i], type='species', weight=abuns)
                    G.add_edge(ProdR1[i], RName[rc-1], edge_color='black')
                    # Check if the 2nd reactant is a valied species:
                    G = check_r2(G, ProdR1[i], ProdR2[i], RName[rc-1],
                                model.spec,model.abuns[:, nt])
                    # Add the reaction products products with abundance:
                    abuns = model.abuns[model.spec.index(ProdP1[i]),nt]
                    G.add_node(ProdP1[i], type='species', weight=abuns)
                    G.add_edge(RName[rc-1], ProdP1[i], color='blue')
                    if ProdP2[i] in model.spec:
                        abuns = model.abuns[model.spec.index(ProdP2[i]),nt]
                        G.add_node(ProdP2[i], type='species', weight=abuns)
                        G.add_edge(RName[rc-1], ProdP2[i], color='blue')
                    if ProdP3[i] in model.spec:
                        abuns = model.abuns[model.spec.index(ProdP3[i]),nt]
                        G.add_node(ProdP3[i], type='species', weight=abuns)
                        G.add_edge(RName[rc-1], ProdP3[i], color='blue')
                    if ProdP4[i] in model.spec:
                        abuns = model.abuns[model.spec.index(ProdP4[i]),nt]
                        G.add_node(ProdP4[i], type='species', weight=abuns)
                        G.add_edge(RName[rc-1], ProdP4[i], color='blue')
                    rc = rc + 1 # raise the reaction counter
            # Now work with the destruction rates:
            # Local copies of the reaction database for the selected species
            # and time step:
            DestRate = model.pdrates[icc].DestRate[:,nt]
            DestRateCoef = model.pdrates[icc].DestRateCoef[:,nt]
            DestRateTot = model.pdrates[icc].DestRateTot[nt]
            DestR1 = model.pdrates[icc].DestR1
            DestR2 = model.pdrates[icc].DestR2
            DestP1 = model.pdrates[icc].DestP1
            DestP2 = model.pdrates[icc].DestP2
            DestP3 = model.pdrates[icc].DestP3
            DestP4 = model.pdrates[icc].DestP4
            # Reorder the destruction reactions to represent the current
            # timestep:
            DestWeight = DestRate / DestRateTot
            OrderIndexDst = DestWeight.argsort()
            OrderIndexDst = OrderIndexDst[::-1]  # reverse the list!
            OrderIndexDst = np.extract(DestWeight[OrderIndexDst] >= thrsh, OrderIndexDst)
            DestRate = DestRate[OrderIndexDst]
            DestRateCoef = DestRateCoef[OrderIndexDst] # Not used at the moment!
            DestWeight = DestWeight[OrderIndexDst]
            DestR1 = al_commons.sortbynparr(DestR1,OrderIndexDst)
            DestR2 = al_commons.sortbynparr(DestR2,OrderIndexDst)
            DestP1 = al_commons.sortbynparr(DestP1,OrderIndexDst)
            DestP2 = al_commons.sortbynparr(DestP2,OrderIndexDst)
            DestP3 = al_commons.sortbynparr(DestP3,OrderIndexDst)
            DestP4 = al_commons.sortbynparr(DestP4,OrderIndexDst)
            nDest = len(DestRate)
            # Add the destruction reactions to the graph:
            for i in range(min([nDest,ntop])):
                rweight = DestWeight[i]
                NewSpecList.append(DestR1[i])
                NewSpecList.append(DestR2[i])
                NewSpecList.append(DestP1[i])
                NewSpecList.append(DestP2[i])
                NewSpecList.append(DestP3[i])
                NewSpecList.append(DestP4[i])
                krTmp = "k"+str(rc)
                TxtTmp = "{0:6s} + {1:6s} -> {2:6s} + {3:6s}   {4:8.2E}".format(
                          DestR1[i], DestR2[i], DestP1[i], DestP2[i],
                          DestRate[i])
                if TxtTmp not in RDetail:
                    RName.append(krTmp)
                    RDetail.append(TxtTmp)
                    G.add_node(RName[rc-1], type='reaction', color='red',
                               weight=rweight)
                    abuns = model.abuns[model.spec.index(DestR1[i]),nt]
                    G.add_node(DestR1[i], type='species', weight=abuns)
                    G.add_edge(DestR1[i], RName[rc-1], edge_color='black')
                    # Check whether the 2nd reactant is valied species:
                    G = check_r2(G, DestR1[i], DestR2[i], RName[rc-1],
                                model.spec, model.abuns[:,nt])
                    # Add the reaction products:
                    abuns = model.abuns[model.spec.index(DestP1[i]),nt]
                    G.add_node(DestP1[i], type='species', weight=abuns)
                    G.add_edge(RName[rc-1], DestP1[i], color='red')
                    if DestP2[i] in model.spec:
                        abuns = model.abuns[model.spec.index(DestP2[i]),nt]
                        G.add_node(DestP2[i], type='species', weight=abuns)
                        G.add_edge(RName[rc-1], DestP2[i], color='red')
                    if DestP3[i] in model.spec:
                        abuns = model.abuns[model.spec.index(DestP3[i]),nt]
                        G.add_node(DestP3[i], type='species', weight=abuns)
                        G.add_edge(RName[rc-1], DestP3[i], color='red')
                    if DestP4[i] in model.spec:
                        abuns = model.abuns[model.spec.index(DestP4[i]),nt]
                        G.add_node(DestP4[i], type='species', weight=abuns)
                        G.add_edge(RName[rc-1], DestP4[i], color='red')
                    rc = rc + 1 # raise the reaction counter
    # The set of unique species involved in the here added reactions is saved
    # and can be used later to iteratively add their reactions to the graph.
    NewSpecList = set(NewSpecList)
    return list(NewSpecList), RName, RDetail

def check_r2(G, r1, r2, k, species, abuns):
    """ Checks the 2nd reactant and adds its node or edge accordingly its type.

    If the second reactant is in the given species list then a node and a black
    edge are added. If the second reactant is not in the species list, but one
    of CRPHOT, CRP, FREEZE or DESORB strings then an edge with different
    colourswill be added. Otherwise a black edge is added (from the 1st
    reactant to the reaction node). The codes return the modified input graph.

    :param G: pygraphviz graph object to which the node and/or edge is added
    :type G: pygraphviz.graph
    :param r1: name of the first reactant
    :type r1: str
    :param r2: name of the second reactant
    :type r2: str
    :param k: identifier of the reaction (i.e. for the 1st it is k1)
    :type k: str
    :param species: list of species included in the chemical network
    :type species: str list
    :param abuns:  abundance of molecules in species
    :type abuns: numpy.ndarray

    :returns: Graph updated with the second reactant.
    """
    if r2 in species:
        abuns = abuns[species.index(r2)]
        G.add_node(r2, type='species', weight=abuns)
        G.add_edge(r2, k, edge_color='black')
    else:
        if r2 == 'CRPHOT' or r2 == 'CRP':
            G.add_edge(r1,k,edge_color='brown')
        elif r2 == 'FREEZE':
            G.add_edge(r1,k,edge_color='cyan')
        elif r2 == 'DESORB':
            G.add_edge(r1,k,edge_color='orange')
        else:
            G.add_edge(r1,k,edge_color='black')
    return G

def draw_chem_graph(model, species="CO", nt=99, ntop=5, depth=1, thrsh=0.1):
    """ Plots the graph of the reactions determining the selected species.

    *Analysis tool* to determine the major reactions and species contributing
    to the production and destruction of a list of target species. The function
    needs to be provided with an ALCHEMIC chemical model that also contains the
    reaction metadata and reaction rate (coefficients) as a function of time.

    The dominant chemical reactions change with *time*, therefore the temporal
    dimention of the problem can be explored by changing the nt argument. If
    this is greater than the number of timesteps, then the last timestep will
    be analised.

    The number of reactions/species shown can be restricted by changing the
    ntop, depth and thrsh variables.

    :param model: PyChemic model containing both abundances and reaction rates
    :type model: pychemic.ChemModl
    :param species: list of species to be analyzed in the graph (default CO)
    :type species: str or list
    :param nt: time step of the simulation to be plotted (default 100)
    :type nt: int
    :param ntop: number of important destruction and production reactions to be
            added to G
    :type ntop: int
    :param depth: recursive depth of the network to be shown (default 1)
    :type depth: int
    :param thrsh: reactions contributing more than thrsh are used (default 0.01)
    :type thrsh: float or double

    :return: Visual representation of the graph created for the specified
        molecule(s) on the screen and in png image.
    """
    # initialize the graph and the figure to be shown
    G=pgv.AGraph(strict=True, directed=True)
    plt.figure(figsize=(20,15), dpi=150)
    # check whether string or list is given
    if type(species) == str:
        species = [species]
    # add the nodes and edges between them
    for jj in range(len(species)):
        index = model.spec.index(species[jj])
        nt = min([nt, (model.nt-1)])
        if jj == 0:
            SpecList, RName, RDetail = add2graph(G, model, species[jj], nt=nt,
                ntop=ntop, RName=[], RDetail=[], thrsh=thrsh)
        else:
            SpecList, RName, RDetail = add2graph(G, model, species[jj], nt=nt,
                ntop=ntop, RName=RName, RDetail=RDetail, thrsh=thrsh)
        if depth > 1: # recursively add species to the graph
            newspec = SpecList
            SpecList =[]
            for ii in range(depth-1):
                for ll in range(len(newspec)):
                    SpecList0, RName, RDetail = add2graph(G, model, newspec[ll],
                        nt=nt, ntop=ntop, RName=RName, RDetail=RDetail,
                        thrsh=thrsh)
                    SpecList = [SpecList,SpecList0]
    # take care of the legend using invisible handles and pyplot legend:
    handlers = []
    for i in range(len(RName)):
        handlers.append(mpatches.Rectangle((0,0), 0, 0, label=RName[i] + ": " +
                        RDetail[i], alpha=0.0))
    # create label for the graph, that contains the time and species analyzed:
    time = model.time[nt]
    if time >= 1e4:
        GraphTime = "%.2f Myr" % (time / 1.0e6)
    else:
        GraphTime = "%.2f yr" % (time)
    if len(species) == 1:
        label = species[0]
        fnamelabel = species[0]
    else:
        label = ''
        fnamelabel = ''
        for jj in range(len(species)-1):
            label = label + species[jj] + ', '
            fnamelabel = fnamelabel + species[jj] + '_'
        label = label + species[-1]
        fnamelabel = fnamelabel + species[-1]
    G.graph_attr['label'] = 'The network of ' + label + ' at ' + GraphTime
    G.node_attr['shape'] = 'circle'
    G.node_attr['fixedsize'] = True
    # set node properties based on their type
    Nodes = G.nodes() # list of nodes
    for i in range(len(Nodes)):
        n = G.get_node(Nodes[i])
        if (n.attr['type'] == 'reaction'):
            n.attr['style'] = 'filled'
            n.attr['fontcolor'] = 'white'
            w = float(n.attr['weight'])
            if w >= 0.5:
                n.attr['width'] = 0.6
                n.attr['fontsize'] = 20
            elif w >= 0.1:
                n.attr['width'] = 0.5
                n.attr['fontsize'] = 17
            elif w >= 0.01:
                n.attr['width'] = 0.4
                n.attr['fontsize'] = 14
            elif w >= 0.001:
                n.attr['width'] = 0.3
                n.attr['fontsize'] = 11
            elif w >= 0.0001:
                n.attr['width'] = 0.2
                n.attr['fontsize'] = 9
            else:
                n.attr['width'] = 0.2
                n.attr['fontsize'] = 9
        if (n.attr['type'] == 'species'):
            lw = np.log10(float(n.attr['weight']))
            if str(n) in set(species):
                n.attr['fillcolor'] = 'yellow'
                n.attr['style'] = 'filled'
            if lw >= -4:
                n.attr['width'] = 0.7
                n.attr['fontsize'] = 22
            elif lw > -5:
                n.attr['width'] = 0.65
                n.attr['fontsize'] = 20
            elif lw > -6:
                n.attr['width'] = 0.6
                n.attr['fontsize'] = 18
            elif lw > -7:
                n.attr['width'] = 0.5
                n.attr['fontsize'] = 10
            elif lw > -8:
                n.attr['width'] = 0.4
                n.attr['fontsize'] = 9
            else:
                n.attr['width'] = 0.35
                n.attr['fontsize'] = 9
    # Create the layout using the GraphViz algorithm
    G.layout(prog='dot')
    # Write out and visualize the results:
    filename = fnamelabel + 'network_nt' + str(nt).strip() + '.png'
    if os.path.isfile( filename ):
           print 'Image', filename, 'already exists!'
           What2Do = raw_input('Do you want to overwrite? (y/n)')
           if What2Do != 'y' and What2Do != 'yes':
              raise Exception('No file was written. Cannot continue!')
    G.draw(filename)
    img = mpimg.imread(filename)
    imgplot = plt.imshow(img)
    plt.legend(handles=handlers, bbox_to_anchor=(1.05, 1), loc=2,
               borderaxespad=0., handlelength=-0.5)
    plt.axis('off')
    plt.show
    plt.savefig(fnamelabel + 'network_nt' + str(nt).strip() + '.eps')

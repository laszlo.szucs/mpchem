pro convert_species_network_char12, rspecies, rreactions,read_format=read_format,$
     write_format=write_format
; This function reads the old CHARACTER*8 type rreaction and rspecies
; files and converts them to the new format: CHARACTER*12
IF NOT KEYWORD_SET(rspecies) THEN rspecies = 'rspecies_OSU06XR_upd_nov2010.dat'
rsn_tmp = strsplit(rspecies,'.',/EXTRACT)
rspecies_new = rsn_tmp[0] + '_long.' + rsn_tmp[1]
IF NOT KEYWORD_SET(rreactions) THEN rreactions = 'rreactions_OSU06XR_upd_nov2010.dat'
rrn_tmp = strsplit(rreactions,'.',/EXTRACT)
rreactions_new = rrn_tmp[0] + '_long.' + rrn_tmp[1]
IF NOT KEYWORD_SET(read_format) THEN read_format = '(I5,1x,2(A8),8x,5(A8),3(E9.2),1x,I2)'
IF NOT KEYWORD_SET(write_format) THEN write_format = '(I6,1X,2(A-12),12X,5(A-12),3E9.2,1x,i2)'

; Chemical species
openr,10,rspecies
 nspecies = 0L
 READF,10,nspecies
 sname = replicate('',nspecies)
 READF,10,sname
close,10

openw,11,rspecies_new
  PRINTF,11,nspecies
  FOR i=0, nspecies-1 DO BEGIN
     PRINTF,11,STRCOMPRESS(sname[i],/REMOVE_ALL), FORMAT='(A-12)'
  ENDFOR
close,11

; Reaction network
openr,20,rreactions
 tmp = ''
 READF,20,tmp
 nreac = ulong((strsplit(tmp,/EXTRACT))[0]) 
 ID = replicate(0L,nreac)
 r1 = replicate('',nreac)
 r2 = replicate('',nreac)
 p1 = replicate('',nreac)
 p2 = replicate('',nreac)
 p3 = replicate('',nreac)
 p4 = replicate('',nreac)
 p5 =  replicate('',nreac)
 alpha = dblarr(nreac)
 beta = dblarr(nreac)
 gamma = dblarr(nreac)
 rtype = replicate(0L,nreac)
 print,  nreac
 FOR ii=0, nreac-1 DO BEGIN
    a=0
    b=''
    c=''
    d=''
    e=''
    f=''
    g=''
    h=''
    i=0.0
    j=0.0
    k=0.0
    l=0
    READF,20,a,b,c,d,e,f,g,h,i,j,k,l,$
           FORMAT=read_format
  ; I5,1X,2(A8),8X,5(A8),3(1pE9.2),1x,i2)
    ID[ii] = a
    r1[ii] = b
    r2[ii] = c
    p1[ii] = d
    p2[ii] = e
    p3[ii] = f
    p4[ii] = g
    p5[ii] = h
    alpha[ii] = i
    beta[ii] = j
    gamma[ii] = k
    rtype[ii] = l
 ENDFOR
close,20

openw,21,rreactions_new 
   PRINTF,21,nreac,"Original", FORMAT='(I-8,A-12)'
   FOR i=0, nreac-1 DO BEGIN
      PRINTF,21,ID[i],r1[i],r2[i],p1[i],p2[i],p3[i],p4[i],p5[i],alpha[i],beta[i],gamma[i],rtype[i],$
            FORMAT=write_format
   ENDFOR
close,21
END

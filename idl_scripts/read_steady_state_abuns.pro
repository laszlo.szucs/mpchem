;
; This function takes the time evolution of the abundance of a species.
; It find the time when the equilibrium abundance is reached. The criterion is:
;
;       1.0-epsilon <= abuns[i]/abuns[i+1] <= 1.0+epsilon
;
; The procedure starts at the final abundance (last time step), and moves backwards
; to find the earliest time at which the criterion still applies.
;
; The function returns a structure with the equilibrium time in years and the 
; Equilibrium abundance.
;
FUNCTION equilibrium_abuns, abuns, time, epsilon=epsilon

     IF NOT KEYWORD_SET(epsilon) THEN epsilon = 0.02

     Nt = n_elements(abuns)
     current = 0d0
     current_time = 0d0
     slope = dblarr(Nt)
     dabuns = dblarr(Nt)
     
     i = Nt - 2
     
     WHILE  ( i gt 0. ) DO BEGIN
     
         dabuns[i] =  abuns[i] / abuns[i+1]
                  
         IF (dabuns[i] GT 1.+epsilon) OR (dabuns[i] LT 1.-epsilon) THEN BEGIN

            current = abuns[i-1]
            current_time = time[i-1]
            BREAK

         ENDIF
         
         i = i-1
     
     ENDWHILE

     result = {eqtime: current_time, eqabuns: current}

     RETURN, result
     
END


;
; This procedure collects ALCHEMIC idl outputs from the current folder. 
; The each file should contain the abundance evolution of single time steps of the 
; dynamic (GADGET) simulation.
;
; For each of these conditions, the equilibrium abundance and the time required
; to reach equilibrium is found. This is then saved to a standard IDL .sav file.
; The filename referes to the particle ID.
; 
; Besides the .sav output, you can also give a variable name in the procedure 
; input. The result structure will be passed to this variable.
;

@read_alchemic.pro

PRO Read_SteadyState_Abuns, results, epsilon=epsilon, sav=sav

    file_list = file_search('pdr_*.idl')

    tmp = read_alchemic(file_list[0])

    Nt = n_elements(file_list)
    Ns = n_elements(tmp.s)
    
    id = lonarr(Nt)
    times = dblarr(Nt)
    spec = tmp.s
    EQabuns = dblarr(Nt, Ns)
    EQtime = dblarr(Nt, Ns)
    
    FOR i=0, Nt-1 DO BEGIN
       tmp = read_alchemic(file_list[i])
       id = tmp.id
       
       FOR j=0, Ns-1 DO BEGIN
       
           tmp2 = equilibrium_abuns(tmp.abundances[*,j],tmp.times,epsilon=epsilon)
           EQabuns[i,j] = tmp2.eqabuns
           EQtime[i,j] = tmp2.eqtime
       
       ENDFOR
    ENDFOR

    results = {Nt:Nt, Ns:Ns, spec: spec, id: id, EQabuns:EQabuns, EQtime:Eqtime}

    IF KEYWORD_SET(sav) THEN BEGIN
       save, results, filename=strcompress(str(id[0],FORMAT='(3I0)'),/remove_all) + $
          'SteadyState_abuns.sav'
    ENDIF
END

;
; FUNCTION Read_SteadyState_Abuns
;
; Same as the Read_SteadyState_Abuns procedure, in fact it call it, but this can
; be used within other programs to simply return the steady state abundances for
; e.g. plotting, data on disk is only stored if requested by setting /sav param.
;

FUNCTION Read_SteadyState_Abuns, folder, epsilon=epsilon, sav=sav
 
    IF NOT KEYWORD_SET(folder) THEN folder = '.'

    cd, current = current
    cd, folder

    Read_SteadyState_Abuns, result, epsilon=epsilon, sav=sav

    cd, current
    
    RETURN, result
    
END

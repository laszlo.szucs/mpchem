FUNCTION READ_KROME, file
    nZ = 2
    if not KEYWORD_SET(file) then file = "fort.22"
    ;spec = ["H","ELECTR","H+","HE","HE+","HE++","H-",]

    nlines = file_lines(file)
    t = dblarr(nlines)
    dens = dblarr(nlines)
    Tgas = dblarr(nlines)

    openr,10,file
    for j=0, nlines-1 do begin
       line = ""
       READF, 10, line
       if (line ne "") then begin
          line = strsplit(line, /extract)
          print, " "
          print, line[2], " ", line[3], " ", line[4]
          t[j] = double(line[1])
          dens[j] = double(line[2])
          Tgas[j] = double(line[3])
       endif else nlines = nlines - 1
    endfor
    close,10

    nperZ = nlines / nZ

    t1 = t[0:nperZ-1]
;    t2 = t[nperZ-1:(2*nperZ)-1]
;    t3 = t[(2*nperZ)-1,(3*nperZ)-1]

    dens1 = dens[0:nperZ-1]
;    dens2 = dens[nperZ-1:(2*nperZ)-1]
;    dens3 = dens[(2*nperZ)-1,(3*nperZ)-1]

    Tgas1 = Tgas[0:nperZ-1]
;    Tgas2 = Tgas[nperZ-1:(2*nperZ)-1]
;    Tgas3 = Tgas[(2*nperZ)-1,(3*nperZ)-1]

    plot, dens1, Tgas1, /xlog, /ylog, thick=3
;    oplot, t2, dens1, linestyle=2, thick=3
;    oplot, t3, dens1, linestyle=4, thick=3

    return, {t: t1, dens: dens1, Tgas: Tgas1}
END

!
! This file is part of the MPChem / pychemic project.
! 
! Copyright (C) 2018 Laszlo Szucs <laszlo.szucs@mpe.mpg.de>
! 
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License as
! published by the Free Software Foundation; version 2 of the License.
! 
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!

MODULE physical_conditions
  !
  ! Defines ChemModel container and read/write routines
  !
  ! Example usage:
  !
  !   PROGRAM phys_cond_example
  !
  !     USE physical_conditions
  !
  !     TYPE(ChemModel), DIMENSION(:), ALLOCATABLE :: models
  !     CHARACTER (len=132) :: input_file
  !     INTEGER (kind=4) :: nr_cell
  !     CHARACTER (len=132) :: base_name, folder
  !
  !     input_file = '1environ.inp'
  !     base_name = 'example'
  !     folder = 'results'
  !
  !     CALL read_environ(input_file, 1, 8, models, nr_cell)
  !     CALL write_idl_out(folder, base_name, models, nr_cell)
  !
  !   END PROGRAM phys_cond
  !
  USE constants
  USE param_module
  USE network_module
  USE interp_module
  !
  IMPLICIT NONE
  SAVE
  !
  ! Create a structure to store the initial conditions, physical conditions,
  ! result abundances and reaction rate coefficients
  !
  TYPE ChemModel
    INTEGER (kind=4) :: ID, nr_time, nr_spec, nr_reac
    LOGICAL :: USE_HEALPX, IS_STATIC
    REAL (kind=8), DIMENSION(:), ALLOCATABLE :: x_loc, y_loc, z_loc, rho, Tg,  &
                                                Td, G0, AvSt, AvIS, ZetaCR,    &
                                                ZetaX, fH2_St, fH2_IS, fCO_St, &
                                                fCO_IS, fN2_St, fN2_IS, fC_IS, &
                                                fC_St, dummy1, dummy2, dummy3, &
                                                UV_ISRF, T00, T01, gdens, &
                                                ddens, time, amu
    REAL (kind=8), DIMENSION(:,:), ALLOCATABLE :: AvArr
    REAL (kind=8), DIMENSION(:,:), ALLOCATABLE :: abundances, ak
    REAL (kind=8), DIMENSION(:), ALLOCATABLE :: xr_ephot, xr_nphot
    ! Arrays of spline interpolation coefficients 
    ! (used in solver for time dependent models)
    ! First dimension: (1:nr_reac) reaction rate coefficients
    !                  (nr_reac+1:nr_reac+nr_phys_spline) physical conditions
    ! Second dimension: time
    INTEGER (kind=4) :: nr_phys_spline
    REAL (kind=8), DIMENSION(:,:), ALLOCATABLE :: spline_b, spline_c, spline_d
  END TYPE ChemModel
  !
CONTAINS
  !
  SUBROUTINE read_environ( input, models, nr_cell )
    !
    ! This subroutine reads initial conditions from ascii file and
    ! stores them in ChemModel structure.
    !
    ! Input parameter(s):
    !
    ! input_file == file containing initial conditions (default is 1environ.inp)
    !
    ! istart     == index of first cell to be returned (set in param_module)
    ! iend       == index of last cell to be returned (set in param_module)
    !
    ! Output parameter(s):
    !
    ! models     == ChemModel variable, this will contain the read data
    ! nr_cell    == number of cells read into mods
    !
    ! Local variables
    CHARACTER (len=*), INTENT(in), OPTIONAL :: input
    INTEGER (kind=4), INTENT(inout) :: nr_cell
    TYPE(ChemModel), DIMENSION(:), ALLOCATABLE, INTENT(out) :: models
    TYPE(ChemModel), DIMENSION(:), ALLOCATABLE :: mods
    CHARACTER (len=90) :: input_file
    INTEGER (kind=4) :: nr_time, input_type, nr_step, nr_read
    INTEGER (kind=4) :: i, j, k, static_int
    REAL (kind=8) :: tstart, tend, tstep
    LOGICAL :: FILE_EXIST
    !
    ! Format codes
    63 FORMAT ( "WARN: Initial species ",(a8)," not found in network!" )
    64 FORMAT ( "INFO: Input file ",(a)," found. Reading physical conditions..." )
    66 FORMAT ( "INFO: ", (a) )
    65 FORMAT ( "ERROR: Input file ",(a)," not found. Stopping!" )
    67 FORMAT ( "INFO: iend = ",(i8)," > nr_cell = ",(i8) )
    ! Codes for physical conditions read in
    68 FORMAT (I8, 20(2X, 1pE11.4))  ! always needed
    69 FORMAT (48(2X, 1pE11.4))      ! HealPix data
    !
    IF ( .NOT. PRESENT(input) ) THEN
       input_file = '1environ.inp'
    ELSE
        input_file = input
    END IF
    !
    ! Check whether file exists
    INQUIRE( FILE=input_file, EXIST=FILE_EXIST )
    !
    IF ( FILE_EXIST ) THEN
       !
       WRITE(stdo,64,advance='no') TRIM(input_file)
       !
       ! Read environment conditions file
       !
       OPEN (unit=01,file=TRIM(input_file), status='old', &
            access='sequential')
       READ (01,*)
       !
       ! Lets extend the nr_cell loops with nr_time: physical conditions
       READ (01,*) input_type  ! Currently not used
       READ (01,*) nr_cell
       ! Warn if iend > nr_cell
       IF (iend.GT.(nr_cell)) then
          WRITE (stdo,*)
          WRITE (stdo,66) 'Possibly wrong position of final grid cell:'
          WRITE (stdo,67) iend, nr_cell
          WRITE (stdo,66,advance='no') "Continue reading physical conditions..."
       END IF
       !
       ! Initialize model database
       ALLOCATE(mods(nr_cell))
       !
       ! Loop over the cells
       DO i = 1, nr_cell, 1
          !
          READ (01,*) nr_time, static_int
          !
          IF (static_int .eq. 1) THEN
             mods(i)%IS_STATIC = .TRUE.
             nr_read = 1
          ELSE
             mods(i)%IS_STATIC = .FALSE.
             nr_read = nr_time
          END IF
          !
          ! Initialize model arrays within the database
       ALLOCATE(mods(i)%x_loc(nr_time), mods(i)%y_loc(nr_time),  &
                mods(i)%z_loc(nr_time), mods(i)%rho(nr_time),    &
                mods(i)%Tg(nr_time), mods(i)%Td(nr_time),        &
                mods(i)%G0(nr_time), mods(i)%AvST(nr_time),      &
                mods(i)%AvIS(nr_time), mods(i)%ZetaCR(nr_time),  &
                mods(i)%ZetaX(nr_time), mods(i)%fH2_IS(nr_time), &
                mods(i)%fH2_St(nr_time), mods(i)%fCO_IS(nr_time),&
                mods(i)%fCO_St(nr_time), mods(i)%fN2_IS(nr_time),&
                mods(i)%fN2_St(nr_time),  mods(i)%fC_St(nr_time),&
                mods(i)%fC_IS(nr_time), mods(i)%dummy1(nr_time),&
                mods(i)%dummy2(nr_time), mods(i)%dummy3(nr_time),&
                mods(i)%UV_ISRF(nr_time), mods(i)%T00(nr_time),  &
                mods(i)%T01(nr_time), mods(i)%gdens(nr_time),    &
                mods(i)%ddens(nr_time), mods(i)%time(nr_time),  &
                mods(i)%amu(nr_time) )
          IF ( USE_HEALPX ) ALLOCATE( mods(i)%AvArr(48,nr_time) )
          !
          ! Save important cell data
          mods(i)%nr_time = nr_time
          mods(i)%USE_HEALPX  = USE_HEALPX
          mods(i)%amu(:) = amu
          !
          ! Loop over the physical timesteps
          !
          DO j = 1, nr_read
             !
             READ (01, 68, ADVANCE='no') mods(i)%ID, mods(i)%x_loc(j),        &
                         mods(i)%y_loc(j), mods(i)%z_loc(j),  &
                         mods(i)%rho(j), mods(i)%Tg(j),       &
                         mods(i)%Td(j), mods(i)%G0(j),        &
                         mods(i)%AvST(j), mods(i)%AvIS(j),    &
                         mods(i)%ZetaCR(j), mods(i)%ZetaX(j), &
                         mods(i)%fH2_St(j), mods(i)%fCO_St(j),&
                         mods(i)%fH2_IS(j), mods(i)%fCO_IS(j),&
                         mods(i)%T00(j), mods(i)%T01(j),      &
                         mods(i)%dummy1(j), mods(i)%dummy2(j),&
                         mods(i)%dummy3(j)
             !
             ! Does it contain HealPX data?
             IF ( USE_HEALPX ) READ (01, 69, ADVANCE='no') mods(i)%AvArr(1:48,j)
             !
             ! Progress to next line in file
             READ (01, *)
             !
             ! Scale some input data
             !
             ! Initialize the starting and end times
             tstart = mods(i)%T00(j) * year
             tend   = mods(i)%T01(j) * year
             mods(i)%T00(j) = tstart
             mods(i)%T01(j) = tend
             mods(i)%time(j) = mods(i)%T00(j)
             ! Ionization by short-living radionuclides:
             mods(i)%ZetaCR(j) = mods(i)%ZetaCR(j) * CRP_scale + ZetaRN
             ! Rescale X-ray ionization rate/luminosity:
             mods(i)%ZetaX(j) = mods(i)%ZetaX(j) * X_scale
             !
             mods(i)%gdens(j) = mods(i)%rho(j) / aMp / amu
             mods(i)%ddens(j) = mods(i)%rho(j) * dust_to_gas          &
                                / (4.0D0/3.0D0 * PI * grain_radius**3 &
                                * grain_rho_bulk)
             !
             ! Initialize data not contained in ascii input
             mods(i)%fC_IS(j) = 1
             mods(i)%fC_St(j) = 1
             mods(i)%fN2_IS(j) = 1
             mods(i)%fN2_St(j) = 1
             !
             ! loop over time steps
          END DO
          !
          ! Set up static model time steps and physical conditions
          !
          IF ( mods(i)%IS_STATIC ) THEN
             ! Avoid dividing by zero
             tstart = 1.00D0 * year
             !
             tstep = 1.00D+01**( DLOG10(tend/tstart) / REAL(nr_time-1) )
             !
             mods(i)%time(1) = tstart
             !
             DO j = 2, nr_time
                !
                mods(i)%time(j) = mods(i)%time(j-1) * tstep
                !
             END DO
             !
             mods(i)%time(1) = 0.0D0
             !
             !
             mods(i)%x_loc(2:nr_time)  = mods(i)%x_loc(1)
             mods(i)%x_loc(2:nr_time)  = mods(i)%y_loc(1)
             mods(i)%x_loc(2:nr_time)  = mods(i)%z_loc(1)
             mods(i)%rho(2:nr_time)    = mods(i)%rho(1)
             mods(i)%Tg(2:nr_time)     = mods(i)%Tg(1)
             mods(i)%Td(2:nr_time)     = mods(i)%Td(1)
             mods(i)%G0(2:nr_time)     = mods(i)%G0(1)
             mods(i)%AvST(2:nr_time)   = mods(i)%AvST(1)
             mods(i)%AvIS(2:nr_time)   = mods(i)%AvIS(1)
             mods(i)%ZetaCR(2:nr_time) = mods(i)%ZetaCR(1)
             mods(i)%ZetaX(2:nr_time)  = mods(i)%ZetaX(1)
             mods(i)%fH2_St(2:nr_time) = mods(i)%fH2_St(1)
             mods(i)%fCO_St(2:nr_time) = mods(i)%fCO_St(1)
             mods(i)%fH2_IS(2:nr_time) = mods(i)%fH2_IS(1)
             mods(i)%fCO_IS(2:nr_time) = mods(i)%fCO_IS(1)
             mods(i)%fC_IS(2:nr_time) = mods(i)%fC_IS(1)
             mods(i)%fC_St(2:nr_time) = mods(i)%fC_St(1)
             mods(i)%fN2_IS(2:nr_time) = mods(i)%fN2_IS(1)
             mods(i)%fN2_St(2:nr_time) = mods(i)%fN2_St(1)
             mods(i)%T00(2:nr_time)    = mods(i)%T00(1)
             mods(i)%T01(2:nr_time)    = mods(i)%T01(1)
             mods(i)%dummy1(2:nr_time) = mods(i)%dummy1(1)
             mods(i)%dummy2(2:nr_time) = mods(i)%dummy2(1)
             mods(i)%dummy3(2:nr_time) = mods(i)%dummy3(1)
             mods(i)%UV_ISRF(2:nr_time)= mods(i)%UV_ISRF(1)
             !
             IF ( USE_HEALPX ) THEN
                DO j = 2, nr_time
                   mods(i)%AvArr(1:48,j) = mods(i)%AvArr(1:48,1)
                END DO
             END IF
             !
             mods(i)%ZetaCR(2:nr_time) = mods(i)%ZetaCR(1)
             mods(i)%ZetaX(2:nr_time)  = mods(i)%ZetaX(1)
             !
             mods(i)%gdens(2:nr_time)  = mods(i)%gdens(1)
             mods(i)%ddens(2:nr_time)  = mods(i)%ddens(1)
             !
          END IF
          
          !
          ! Set data used directly by the solver
          !
          mods(i)%nr_spec = network%nr_spec
          mods(i)%nr_reac = network%nr_reac
          !
          ! Initialize abundances and rate coefficients
          ALLOCATE( mods(i)%abundances(network%nr_spec, nr_time),  &
                    mods(i)%ak(network%nr_reac, nr_time) )
          ! 
          ! Initialize spline coefficient arrays for non-static models
          IF ( .not. mods(i)%IS_STATIC ) THEN
             !
             CALL init_spline_coef( mods(i) )
             !
          END IF

          !
          ! Set initial abundances
          !
          DO j = 1, nr_init_spec
             !
             CALL ispecies( init_spec(j), network%nr_spec, network%spec_name, k)
             IF (k.eq.0) THEN
                IF (i.eq.1) THEN
                   WRITE(stdo,*)
                   WRITE(stdo,63) init_spec(j)
                   WRITE (stdo,66,advance='no') "Continue reading physical conditions..."
                END IF
             ELSE
                mods(i)%abundances(k,1) = init_abuns(j)
             END IF
             !
          END DO
          !
          ! loop over cells
       END DO
       !
       WRITE(stdo,'(2x,a)') 'DONE'
       !
    ELSE
       !
       WRITE(stdo,65) TRIM(input_file)
       STOP
       !
    END IF
    !
    ! Select requested cells in (istart,iend) range
    ! TODO: Don't read unnecessary data from file
    iend = MIN(iend,nr_cell)
    nr_cell = iend-istart+1
    !
    ! Prepare requested data
    ALLOCATE( models(nr_cell) )
    !
    models(:) = mods(istart:iend)
    !
    ! Done
    RETURN
    !
  END SUBROUTINE read_environ
  !
  SUBROUTINE write_idl_out( folder, base_name, mods, nr_cell )
    !
    ! This subroutine writes results stored in ChemModel structure 
    ! to IDL and python (PyChemic) readable file.
    !
    ! Input parameter(s):
    !
    ! folder     == output files are written to folder. (It must exist!)
    ! base_name  == string used for constructing the output filename
    ! mods       == array constaning ChemModel structures to be writen
    ! nr_cell    == number of ChemModel structures in mods
    !
    ! Output parameter(s):
    !
    ! IDL and python readable files (nr_cell individual) in <folder> 
    ! with following file name structure:
    !
    !      <base_name>_<cell ID>.idl
    !
    !
    ! Local variable(s)
    CHARACTER (len=*), INTENT(in) :: folder, base_name
    TYPE(ChemModel), DIMENSION(:),INTENT(in) :: mods
    INTEGER (kind=4), INTENT(in) :: nr_cell
    INTEGER (kind=4) :: ipon = 2   ! legacy variable
    INTEGER (kind=4) :: i, j, l, nr_time, nr_spec, nr_reac
    CHARACTER (len=132) :: out_name, fout
    !
    ! Check/create output directory
    CALL check_folder( TRIM(folder) )
    !
    ! Write out info
    !
    WRITE(stdo,'("INFO: Writing ",I5," cells to IDL output...")',advance='no') nr_cell
    !
    ! Loop over cells
    !
    DO i = 1, nr_cell, 1
       !
       WRITE(fout,'(I8.8)') mods(i)%ID
       !
       ! Construct the output file name
       out_name = TRIM(folder)//'/'//TRIM(base_name)//'_'//TRIM(fout)//'.idl'
       !
       OPEN (unit=30, file=out_name, status='unknown', access='append')
       REWIND 30
       !
       nr_time = mods(i)%nr_time
       nr_spec = mods(i)%nr_spec
       nr_reac = mods(i)%nr_reac
       !
       ! Write current parameters to the 'out//.idl':
       WRITE(30,'(A18)') '# ART input file: '
       WRITE(30,'(a80)') species_file !specs
       WRITE(30,'(a80)') reaction_file !rates
       WRITE(30,'(I5)') ipon
       WRITE(30,'(I8)') mods(i)%ID
       WRITE(30,'(1PE9.2)') mods(i)%x_loc(1)  ! TODO: save complete array!
       WRITE(30,'(1PE9.2)') mods(i)%y_loc(1)  ! TODO: save complete array!
       WRITE(30,'(1PE9.2)') mods(i)%z_loc(1)  ! TODO: save complete array!
       WRITE(30,'(1PE9.2)') grain_rho_bulk
       WRITE(30,'(1PE9.2)') dust_to_gas
       WRITE(30,'(1PE9.2)') grain_radius
       WRITE(30,'(1PE9.2)') 0.0  !albedo_UV
       WRITE(30,'(1PE9.2)') mods(i)%time(1) / year               !TODO: remove
       WRITE(30,'(1PE9.2)') mods(i)%time(nr_time) / year !TODO: remove
       !
       ! Write the time dependent input parameters:
       WRITE(30,*) nr_time
       WRITE(30,'(10D12.5)') (mods(i)%time(j) / year, j = 1, nr_time)
       WRITE(30,'(10D12.5)') (mods(i)%Tg(j), j = 1, nr_time)
       WRITE(30,'(10D12.5)') (mods(i)%Td(j), j = 1, nr_time)
       WRITE(30,'(10D12.5)') (mods(i)%rho(j), j = 1, nr_time)
       WRITE(30,'(10D12.5)') (mods(i)%AvSt(j), j = 1, nr_time)
       WRITE(30,'(10D12.5)') (mods(i)%AvIS(j), j = 1, nr_time)
       WRITE(30,'(10D12.5)') (mods(i)%G0(j), j = 1, nr_time)
       WRITE(30,'(10D12.5)') (mods(i)%fH2_St(j), j = 1, nr_time)
       WRITE(30,'(10D12.5)') (mods(i)%fCO_St(j), j = 1, nr_time)
       WRITE(30,'(10D12.5)') (mods(i)%fH2_IS(j), j = 1, nr_time)
       WRITE(30,'(10D12.5)') (mods(i)%fCO_IS(j), j = 1, nr_time)
       WRITE(30,'(10D12.5)') (mods(i)%ZetaCR(j), j = 1, nr_time)
       WRITE(30,'(10D12.5)') (mods(i)%ZetaX(j), j = 1, nr_time)
       WRITE(30,'(10D12.5)') (mods(i)%gdens(j), j = 1, nr_time)  ! TODO
       WRITE(30,'(10D12.5)') (mods(i)%amu(j), j = 1, nr_time)      ! TODO
       ! Write the initial and calculated species
       WRITE(30,'(I4)') 5 !nfrc
      WRITE(30,'(A12,1x,D22.15)') (network%spec_name(j), 0.0D0, j = 1, 5)
       WRITE(30,*) nr_spec
       WRITE(30,'((9(A12,2x)),:)') (network%spec_name(j), j = 1, nr_spec)
       ! Write all abundances (nstep*tt_final) to the file
       WRITE(30,*) nr_reac
       WRITE(30,'((9(A12,2x)),:)') (network%r1(j), j = 1, nr_reac)
       WRITE(30,'((9(A12,2x)),:)') (network%r2(j), j = 1, nr_reac)
       WRITE(30,'((9(A12,2x)),:)') (network%p1(j), j = 1, nr_reac)
       WRITE(30,'((9(A12,2x)),:)') (network%p2(j), j = 1, nr_reac)
       WRITE(30,'((9(A12,2x)),:)') (network%p3(j), j = 1, nr_reac)
       WRITE(30,'((9(A12,2x)),:)') (network%p4(j), j = 1, nr_reac)
       WRITE(30,'((9(A12,2x)),:)') (network%p5(j), j = 1, nr_reac)
       WRITE(30,'(10D12.5)') ((mods(i)%ak(j,l), l = 1, nr_time), j = 1, nr_reac)
       WRITE(30,'(10D12.5)') (((mods(i)%abundances(j,l) )+1.00D-99,  &
                                l=1,nr_time), j = 1, nr_spec)
       !
       CLOSE(30)
       !
    END DO
    !
    ! Write out info
    !
    WRITE(stdo,*) 'DONE'
    !
    RETURN
    !
  END SUBROUTINE write_idl_out
  !
  SUBROUTINE write_bin_out( folder, base_name, mods, nr_cell )
    !
    ! This subroutine writes results stored in ChemModel structure 
    ! to unformatted binary output. This is used for fast file access and
    ! large models. The output file contains all cells.
    !
    ! Input parameter(s):
    !
    ! folder     == output file is written to folder. (It must exist!)
    ! base_name  == string used for constructing the output filename
    ! mods       == array constaning ChemModel structures to be writen
    ! nr_cell    == number of ChemModel structures in mods
    !
    ! Output parameter(s):
    !
    ! Unformatted binary file (single) in <folder>, containing all  
    ! cells and ascii file contaning the species list. (Both are 
    ! needed for reading data to IDL or python (PyChemic). The filenames
    ! follow the convention:
    !
    !      <base_name>.bout
    !      <base_name>_species.dat
    !
    !
    ! Local variable(s)
    CHARACTER (len=*), INTENT(in) :: folder, base_name
    TYPE(ChemModel), DIMENSION(:),INTENT(in) :: mods
    INTEGER (kind=4), INTENT(in) :: nr_cell
    INTEGER (kind=4) :: i, j, l, nr_time
    CHARACTER (len=132) :: out_name
    LOGICAL :: FILE_EXIST
    !
    ! Format codes
    61 FORMAT (1x,' NUMBER OF VALID SPECIES USED = ',1i4)
    !
    ! Check/create output directory
    CALL check_folder( TRIM(folder) )
    !
    ! Write out info
    !
    WRITE(stdo,'("INFO: Writing ",I5," cells to binary output...")',advance='no') nr_cell
    !
    ! Construct output file names
    out_name = TRIM(folder)//'/'//TRIM(base_name)
    !
    ! (Over)write ascii file containing species names
    OPEN (unit=31, file=TRIM(out_name)//'_species.dat', status='unknown')
    WRITE(31,61) network%nr_spec
    WRITE(31,'((9(A12,2x)),:)') (network%spec_name(j), j = 1, network%nr_spec)
    CLOSE(31)
    !
    ! Write models stored in mods structure to binary file
    OPEN (unit=30,file=TRIM(out_name)//'.bout', access="STREAM")
    !
    ! Loop over cells
    !
    DO i = 1, nr_cell, 1
       !
       nr_time = mods(i)%nr_time
       !
       ! TODO: reorder output to be consistent with IDL and ascii outputs
       WRITE(30) nr_cell
       WRITE(30) nr_time, network%nr_spec
       WRITE(30) mods(i)%ID
       WRITE(30) mods(i)%time(1:nr_time)
       WRITE(30) mods(i)%x_loc(1:nr_time)
       WRITE(30) mods(i)%y_loc(1:nr_time)
       WRITE(30) mods(i)%z_loc(1:nr_time)
       WRITE(30) mods(i)%rho(1:nr_time)
       WRITE(30) mods(i)%AvSt(1:nr_time)
       WRITE(30) mods(i)%AvIS(1:nr_time)
!        WRITE(30) mods(i)%G0(1:nr_time)
       WRITE(30) mods(i)%Tg(1:nr_time)
       WRITE(30) mods(i)%Td(1:nr_time)
       WRITE(30) mods(i)%ZetaCR(1:nr_time)
!        WRITE(30) mods(i)%ZetaX(1:nr_time)
       WRITE(30) mods(i)%gdens(1:nr_time)
       WRITE(30) mods(i)%abundances(1:network%nr_spec,1:nr_time)
       !
    END DO
    !
    CLOSE(30)
    !
    ! Write out info
    !
    WRITE(stdo,*) 'DONE'
    !
    RETURN
    !
  END SUBROUTINE write_bin_out
  !
  SUBROUTINE write_ascii_out( folder, base_name, mods, nr_cell )
    !
    ! This subroutine writes results stored in ChemModel structure 
    ! to human readable plane ascii file.
    !
    ! Input parameter(s):
    !
    ! folder     == output files are written to folder. (It must exist!)
    ! base_name  == string used for constructing the output filename
    ! mods       == array constaning ChemModel structures to be writen
    ! nr_cell    == number of ChemModel structures in mods
    !
    ! Output parameter(s):
    !
    ! Human readable files (nr_cell individual) in <folder> 
    ! with following file name structure:
    !
    !      <base_name>_<cell ID>.out
    !
    ! Note that these files are not readable in PyChemic.
    !
    !
    ! Local variable(s)
    CHARACTER (len=*), INTENT(in) :: folder, base_name
    TYPE(ChemModel), DIMENSION(:), INTENT(in) :: mods
    INTEGER (kind=4), INTENT(in) :: nr_cell
    INTEGER (kind=4) :: ipon = 1   ! legacy variable
    INTEGER (kind=4) :: i, j, l, nr_time
    INTEGER (kind=4), PARAMETER :: spec_per_line = 6
    CHARACTER (len=132) :: out_name, fout
    !
    ! Check/create output directory
    CALL check_folder( TRIM(folder) )
    !
    ! Write out info
    !
    WRITE(stdo,'("INFO: Writing ",I5," cells to plain text output...")',advance='no') nr_cell
    !
    ! Set up format codes
    !
    20 FORMAT(//)
    21 FORMAT(1x,' SPECIES CONTAINED IN SCHEME: ',//)
    22 FORMAT(6(1x,a12,2x))
    23 FORMAT(/)
    30 FORMAT(1x,7(1pe11.3))
    36 FORMAT(1x,' NUMBER OF VALID SPECIES USED = ',1i4)
    41 FORMAT(3x,'time',7x,6(1a9,3x))
    45 FORMAT(1x,' NUMBER OF VALID REACTIONS USED = ',1i6)
    63 FORMAT(3X,' CALCULATED ABUNDANCES: '/)
    64 FORMAT(3x,' INITIAL VALUES: '/)
    65 FORMAT(                                      &
         3X,' Grid point  = ',I5,/,                 &
         3X,' ID          = ',I8,/,                 &
         3X,' X           = ',1PE11.3,' AU',/,      &
         3X,' Y           = ',1PE11.3,' AU',/,      &
         3X,' Z           = ',1PE11.3,' AU',/,      &
         3X,' n(H+2H2)    = ',1PE11.3,' cm^(-3)',/, &
         3X,' Tg          = ',0PF8.1,'K',/,         &
         3X,' rho_g       = ',1PE11.3,' g/cm^3',/,  &
         3X,' Td          = ',0PF8.1,'K',/,         &
         3X,' rho_d       = ',1PE11.3,' g/cm^3',/,  &
         3X,' Mdust/Mgas  = ',1PE11.3,/,            &
         3X,' grain size  = ',1PE11.3,' cm',/,      &
         3X,' Av(stellar) = ',1PE11.3,' mag.',/,    &
         3X,' Av(IS)      = ',1PE11.3,' mag.',/,    &
         3X,' G0(stellar) = ',1PE11.3,' G0(IS)',/,  &
         3X,' Zeta        = ',1PE11.3,/,            &
         3X,' albedo(UV)  = ',1PE11.3,/,            &
         3X,' Start time  = ',0PF9.0,' years',/,    &
         3X,' Finish time = ',0PF9.0,' years',/,    &
         3X,' Diff. coeff.= ',1PE11.3)
    75 FORMAT(21x,39('*'))
    76  FORMAT(1x,80('-'))
    ! TODO: use the VERSION parameter
    84  FORMAT(  &
         20X,' **             PYCHEMIC              **',/,   &
         20X,' **          ',1I3,' SPECIES SET          **',/, &
         20X,' **            F03 VERSION            **',/,     &
         20X,' **             27/06/2018            **')
    !
    ! Loop over cells
    !
    DO i = 1, nr_cell, 1

       WRITE(fout,'(I8.8)') mods(i)%ID
       !
       ! Construct the output file name
       out_name = TRIM(folder)//'/'//TRIM(base_name)//'_'//TRIM(fout)//'.dat'
       !
       ! Open file for writing
       OPEN (unit=30, file=out_name, status='unknown', access='append')
       REWIND 30
       !
       WRITE(30,20)
       WRITE(30,75)
       WRITE(30,84) network%nr_spec
       WRITE(30,75)
       WRITE(30,20)
       WRITE(30,22) (network%spec_name(j),j=1,network%nr_spec)
       WRITE(30,23)
       WRITE(30,36) network%nr_spec
       WRITE(30,45) network%nr_reac
       WRITE(30,23)
       !
       ! Write input parameters:
       WRITE(30,64)
       !
       WRITE(30,65) ipon, mods(i)%ID, mods(i)%x_loc(1), mods(i)%y_loc(1), &
                    mods(i)%z_loc(1), mods(i)%gdens(1), mods(i)%Tg(1),    &
                    mods(i)%rho(1), mods(i)%Td(1), grain_rho_bulk,        &
                    dust_to_gas, grain_radius, mods(i)%AvSt(1),           &
                    mods(i)%AvIS(1), mods(i)%G0(1),                       &
                    (mods(i)%ZetaCR(1)+mods(i)%ZetaX(1)), albedo_UV,      &
                    mods(i)%time(1)/year,                                & 
                    mods(i)%time(mods(i)%nr_time)/year,                  &
                    0.0d0
       WRITE(30,20)
       !
       ! Write calculated abundances:
       !
       WRITE(30,63)
       !
       ! indexing with spec_per_line steps
       istart = 1
       iend = spec_per_line
       !
       ! Start loop
       32 WRITE(30,41) (network%spec_name(j), j=istart, iend)
       WRITE(30,76)
       !
       ! Write all abundances as function of time
       DO l = 1, mods(i)%nr_time
          WRITE(30,30) mods(i)%time(l) / year,(mods(i)%abundances(j,l),  &
                       j=istart, iend)
       END DO
       !
       WRITE(30,41)(network%spec_name(j),j=istart,iend)
       WRITE(30,20)
       !
       ! Next spec_per_line species to be written
       istart = istart + spec_per_line
       iend = iend + spec_per_line
       !
       ! Interupt when nr_spec is reached
       IF (iend.GT.network%nr_spec) iend = network%nr_spec
       IF (istart.GE.network%nr_spec) GOTO 38
       !
       ! Go back to loop start
       GOTO 32
       !
       ! Finish loop
       38 CONTINUE
       !
       ! Close the ith file
       !
       CLOSE(30)
    END DO
    !
    ! Write out info
    !
    WRITE(stdo,*) 'DONE'
    !
    RETURN
    !
  END SUBROUTINE write_ascii_out
  !
  SUBROUTINE init_spline_coef( model )
     !
     ! Allocates and fills spline coefficient arrays for time dependent 
     ! models. Spline interpolation is used in compute_dependent_ratecoef() 
     ! routine (rates_module). )The spline coefficient arrays are two  
     ! dimensional. The first nr_reac rows in the first dimension will contain 
     ! the reaction rate coefficient data (currently all 0.0 value, it is  
     ! filled in rates_module), the next nr_phys_spline rows contain the  
     ! physical condition data. If further physical conditions need to be  
     ! interpolated in compute_dependent_ratecoef() then add them here.
     ! Note that nr_phys_spline parameter should be updated if more physical 
     ! conditions are interpolated. mpi_com.F90 should automaticaly adjust to 
     ! the new nr_phys_spline parameter.
     ! 
     ! If the model is static, this routine shound not be called.
     ! 
     ! The spline_b, spline_c and spline_d arrays have the folloing meaning:
     !   (1:nr_reac,:)         reaction rate coefficients
     !   (nr_reac+1,:)         number density 
     !   (nr_reac+2,:)         gas temperature
     !   (nr_reac+3,:)         dust temperature
     !   (nr_reac+4,:)         visual extinction, star
     !   (nr_reac+5,:)         visual extinction, ISM
     !   (nr_reac+6,:)         ZetaCR
     !   (nr_reac+7,:)         ZetaX
     !   (nr_reac+8,:)         dust grain number density (ddens)
     !
     ! Input parameter(s):
     !
     ! model                == ChemModel derived datatype variable
     !
     ! Output parameter(s):
     !
     ! model%nr_phys_spline == number of physical condition parameters stored
     !                         in the spline coefficient arrays
     ! model%spline_b       == cubic spline coefficients (b, c, d) of physical 
     ! model%spline_c          conditions to be used for interpolation of 
     ! model%spline_d          abundance dependent rate coefficients in Fcn() 
     !                         routine in solve module
     !
     
     !
     ! Local variable(s)
     TYPE(ChemModel), INTENT(inout) :: model
     REAL (kind=8), DIMENSION(model%nr_time) :: b, c, d
     INTEGER (kind=4), PARAMETER :: nr_phys_spline = 8
     INTEGER (kind=4) :: nr_time, nr_reac
     !
     nr_time = model%nr_time
     nr_reac = model%nr_reac
     
     model%nr_phys_spline = nr_phys_spline
     
     !
     ! Initialise spline coefficient arrays
     IF ( ALLOCATED(model%spline_b) ) DEALLOCATE( model%spline_b )
     IF ( ALLOCATED(model%spline_c) ) DEALLOCATE( model%spline_c )
     IF ( ALLOCATED(model%spline_d) ) DEALLOCATE( model%spline_d )
     !
     ALLOCATE( model%spline_b( nr_reac+nr_phys_spline, nr_time ), &
               model%spline_c( nr_reac+nr_phys_spline, nr_time ), &
               model%spline_d( nr_reac+nr_phys_spline, nr_time ) )
     !
     model%spline_b(:,:) = 0.0D0
     model%spline_c(:,:) = 0.0D0
     model%spline_d(:,:) = 0.0D0
     !
     ! Compute physical condition spline coefficients
     !
     ! Density
     CALL spline( model%time, model%gdens, b, c, d, nr_time )
     model%spline_b( nr_reac+1, 1:nr_time ) = b
     model%spline_c( nr_reac+1, 1:nr_time ) = c
     model%spline_d( nr_reac+1, 1:nr_time ) = d
     !
     ! Gas temperature
     CALL spline( model%time, model%Tg, b, c, d, nr_time )
     model%spline_b( nr_reac+2, 1:nr_time ) = b
     model%spline_c( nr_reac+2, 1:nr_time ) = c
     model%spline_d( nr_reac+2, 1:nr_time ) = d
     !
     ! Dust temperature
     CALL spline( model%time, model%Td, b, c, d, nr_time )
     model%spline_b( nr_reac+3, 1:nr_time ) = b
     model%spline_c( nr_reac+3, 1:nr_time ) = c
     model%spline_d( nr_reac+3, 1:nr_time ) = d
     !
     ! Visual extinction
     CALL spline( model%time, model%AvSt, b, c, d, nr_time )
     model%spline_b( nr_reac+4, 1:nr_time ) = b
     model%spline_c( nr_reac+4, 1:nr_time ) = c
     model%spline_d( nr_reac+4, 1:nr_time ) = d
     !
     ! Visual extinction
     CALL spline( model%time, model%AvIS, b, c, d, nr_time )
     model%spline_b( nr_reac+5, 1:nr_time ) = b
     model%spline_c( nr_reac+5, 1:nr_time ) = c
     model%spline_d( nr_reac+5, 1:nr_time ) = d
     !
     ! Cosmic ray ionisation rate
     CALL spline( model%time, model%ZetaCR, b, c, d, nr_time )
     model%spline_b( nr_reac+6, 1:nr_time ) = b
     model%spline_c( nr_reac+6, 1:nr_time ) = c
     model%spline_d( nr_reac+6, 1:nr_time ) = d
     !
     ! X-ray ionisation rate
     CALL spline( model%time, model%ZetaX, b, c, d, nr_time )
     model%spline_b( nr_reac+7, 1:nr_time ) = b
     model%spline_c( nr_reac+7, 1:nr_time ) = c
     model%spline_d( nr_reac+7, 1:nr_time ) = d
     !
     ! Dust grain number density
     CALL spline( model%time, model%ddens, b, c, d, nr_time )
     model%spline_b( nr_reac+8, 1:nr_time ) = b
     model%spline_c( nr_reac+8, 1:nr_time ) = c
     model%spline_d( nr_reac+8, 1:nr_time ) = d
     
     !
     ! Finish
     RETURN
  END SUBROUTINE init_spline_coef
  !
END MODULE physical_conditions
